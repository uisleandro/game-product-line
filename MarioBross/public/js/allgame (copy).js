'use strict';

var once=function(element,evnt,funct){
	if(element.attachEvent)
	return element.attachEvent("on"+evnt,funct);
	else return element.addEventListener(evnt, funct, false)
};

var __hasProp = {}.hasOwnProperty,
__extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

//begin game
(function() {
  var Game;
  Game = (function() {
    var that;
    
    var scene = null;
    var canvas = null;
    var context = null;
    
    function Game(CanvasId) {
    	that = this;
    	    	
    	//Será atualizado
		canvas = document.getElementById(CanvasId);
		
		//Sera atualizado
		context = canvas.getContext("2d");
		
		if(typeof once === "function"){
			once(window,'resize', function(){
				//coloca a cena e o canvas do tamanho da janela
				that.fitScreen();
			});
		}
		else{
			throw "once is not defined";
			exit(1);
		}
		
		this.now = Date.now();
	    this.then = this.now;
	    
	    this.GAME_RUNNING = false;
		
    }

    /**
     * @Description coloca a cena e o canvas do tamanho da janela
     */
    Game.prototype.fitScreen = function(){
    	canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
		
		if(scene != null){
			
			//scene.resize(canvas.width, canvas.height);
			//scene.fitScreen();
			scene.clearMobs();
			scene.makeScene();
		}
    	else
    		throw "in 'Game' scene is null\nplease use: game.setScene(Scene)";
		
		
		return that;
    }
    
    Game.prototype.setScene = function(Scene){
    	scene = Scene;
    	scene.context = context;
    	that.fitScreen();
    	
    	return that;
    }    
    
    Game.prototype.loop = function() {
    	
    	if(!that.GAME_RUNNING){
    		that.GAME_RUNNING = true;
    		
    		that.now = Date.now();
	    	that.then = that.now;
	    	
    		window.requestAnimationFrame(that.loop.bind(this));
    		
    		return;
    	}
    	
    	that.now = Date.now();
    	var delta = (that.now - that.then);
    	that.then = this.now;
    	
    	if(scene != null){
    		//console.log(delta);
    		scene.draw(delta);
    	}
    	else{
    		throw "in 'Game' scene is null\nplease use: game.setScene(Scene)";
    	}
    	
    	window.requestAnimationFrame(that.loop.bind(this));
    };

    return Game;

  })();

  window.Game = Game;

}).call(this);
//end game
//begin scene
(function() {
	  var Scene;

	  Scene = (function() {
	    var that;
	    
	    
	    //usar para detectar a area onde pode conter colisaõ de acordo coma posicao do char
	    
	    //o indice de area corresponde a um deslocamento do char [0.. blockCount]
	    //o valor de area contem um indice para uma posicao solid.objects[index];
	    //os objetos que tiverem sua posicao nesse indice poderão colidir com
	    //o personagem se o personagem estiver numa posição que corresponde ao seu 
	    //deslocamento / that.BlockWidth
	    //(se for considerado somente uma area horizontal)
	    //var that.SolidAreas = new Array(that.BlockCols * that.BlockRows);
	    
	    //um array para facilitar que os objetos sejam mostrados na tela
	    var definedSolids = new Array();
	    
	    //contem um elemento that.img
	    
	    
	    //obstaculos que compoem o cenario
	    var solid = null;
	    
	    //contexto
	    
	    //o personagem que ira jogar o jogo
	    var player = null;
	    var mobs = [];
	    
	    
	    function Scene(SolidObject) {
	    	that = this;
	    	
	    	this.context = null;
	    	
	    	
	    	this.readyState = false;
	    	
	    	
	    	this.img = new Image();
	    	solid = SolidObject;
	    	
	    	//that.img.src = solid.
	    	
	    	once(that.img,'load', function(){
	    		that.readyState = true;
	    	});
	    	that.img.src = solid.original_name;
	    	
	    	
	    	
	    	this.BlockCols = 10;
	    	this.BlockRows = 6;
	    	
	    	
	    	this.SolidAreas = new Array(this.BlockCols * this.BlockRows);
	    	this.MobAreas = new Array(this.BlockCols * this.BlockRows);
	    	
	    	this.Width = 16*60;
	    	this.Height = 9*60;
	    	
	    	this.scaleX = 1;
	    	this.scaleY = 1;
	    	
	    	this.BlockWidth = this.Width / this.BlockCols;
	    	this.BlockHeight = this.Height / this.BlockRows;
	   	
	    }    
	    
	    Scene.prototype.addPlayer = function(character){
	    	character.scene = this;
	    	player = character;
	    }
	    
	    /*
	     * o mob atualiza sua posição entrando numa àrea
	     * o player verifica se está colidindo dentro da sua própria área
	     * */
	    
	    /**
	     * @Param Character character
	     * */
	    Scene.prototype.addMob = function(col1, row, limitX, distance, translateX, translateY, toRight){
	    	
	    	
	    	
	    	

				var mob = new Mob({
				"type" : "animation-coords",
				"original_name" : "img/mob001.png",
				"original_width" : 116,
				"original_height" : 104,
				"frame_count" : 17,
				"w" : 30,
				"h" : 28,
				"animations" : {
					"esquerda" : {
						"loops" : "0",
						"objects" : [ {
							"id" : 0,
							"delay" : 10,
							"x" : 24,
							"y" : 78,
							"w" : 26,
							"h" : 26,
							"gx" : 1,
							"gy" : 0,
							"gz" : 0
						}, {
							"id" : 1,
							"delay" : 10,
							"x" : 52,
							"y" : 51,
							"w" : 26,
							"h" : 27,
							"gx" : 0,
							"gy" : 0,
							"gz" : 0
						} ]
					},
					"parado" : {
						"loops" : "0",
						"objects" : [ {
							"id" : 0,
							"delay" : 10,
							"x" : 24,
							"y" : 0,
							"w" : 24,
							"h" : 23,
							"gx" : 0,
							"gy" : -2,
							"gz" : 0
						}, {
							"id" : 1,
							"delay" : 10,
							"x" : 0,
							"y" : 23,
							"w" : 24,
							"h" : 24,
							"gx" : 0,
							"gy" : -2,
							"gz" : 0
						}, {
							"id" : 2,
							"delay" : 10,
							"x" : 24,
							"y" : 23,
							"w" : 28,
							"h" : 28,
							"gx" : 0,
							"gy" : -4,
							"gz" : 0
						}, {
							"id" : 3,
							"delay" : 10,
							"x" : 52,
							"y" : 23,
							"w" : 28,
							"h" : 27,
							"gx" : 0,
							"gy" : -4,
							"gz" : 0
						}, {
							"id" : 4,
							"delay" : 10,
							"x" : 80,
							"y" : 23,
							"w" : 30,
							"h" : 26,
							"gx" : 0,
							"gy" : -3,
							"gz" : 0
						}, {
							"id" : 5,
							"delay" : 10,
							"x" : 24,
							"y" : 51,
							"w" : 28,
							"h" : 27,
							"gx" : 0,
							"gy" : -3,
							"gz" : 0
						}, {
							"id" : 6,
							"delay" : 10,
							"x" : 24,
							"y" : 51,
							"w" : 28,
							"h" : 27,
							"gx" : 0,
							"gy" : -3,
							"gz" : 0
						} ]
					},
					"direita" : {
						"loops" : "0",
						"objects" : [ {
							"id" : 0,
							"delay" : 10,
							"x" : 0,
							"y" : 47,
							"w" : 20,
							"h" : 28,
							"gx" : 0,
							"gy" : -4,
							"gz" : 0
						}, {
							"id" : 1,
							"delay" : 10,
							"x" : 0,
							"y" : 75,
							"w" : 24,
							"h" : 22,
							"gx" : 1,
							"gy" : -4,
							"gz" : 0
						}, {
							"id" : 2,
							"delay" : 10,
							"x" : 52,
							"y" : 78,
							"w" : 24,
							"h" : 23,
							"gx" : -1,
							"gy" : -4,
							"gz" : 0
						}, {
							"id" : 3,
							"delay" : 10,
							"x" : 78,
							"y" : 51,
							"w" : 23,
							"h" : 25,
							"gx" : -1,
							"gy" : -3,
							"gz" : 0
						} ]
					}
				}
			});
	    	
	    	
	    	mob.scene = this;
	    	mob.delimite(col1, row, limitX, distance, translateX, translateY, toRight);
	    	mobs[mobs.length] = mob;
	    }
	    
	    
	    Scene.prototype.fitScreen = function(){
	    	
	    	that.SolidAreas.length = 0;
	    	definedSolids.length = 0;
	    	
	    	that.Width = 16*60;
	    	that.Height = 9*60;
	    	
	    	that.scaleX = window.innerWidth / that.Width;
	    	that.scaleY = window.innerHeight / that.Height;
	    	
	    	that.BlockWidth = that.Width / that.BlockCols * that.scaleX;
	    	that.BlockHeight = that.Height / that.BlockRows * that.scaleY;
	    	
	    	that.Width = window.innerWidth;
	    	that.Height = window.innerHeight;
	    	
	    	player.fitScreen();
	    	
	    	for(var i = 0; i < mobs.length; i++){
	    		mobs[i].fitScreen();
	    	}
	    	

	    }
	    
	    Scene.prototype.resize = function(width,height){
	    	
	    	/*  	
	    	that.SolidAreas = new Array(that.BlockCols * that.BlockRows);
	        definedSolids = new Array();
	        */
	    	
	    	//limpa os arrays
	    	that.SolidAreas.length = 0;
	    	definedSolids.length = 0;
	    	
	    	
	    	that.BlockWidth = width / that.BlockCols;
			that.BlockHeight = height / that.BlockRows;
			
			//TODO: pode precisar?
	    	that.Width = width;
	    	that.Height = height;
	    	
	    	//alert(that.BlockWidth);    	
	    }
	    
	    
	    var drawLine = function(context, x1, y1, x2, y2){
			context.beginPath();
			context.moveTo(x1,y1);
			context.lineTo(x2, y2);
			context.stroke();
		}
	    
	    Scene.prototype.drawLine = function(color, x1, y1, x2, y2){
	    	this.context.strokeStyle = color;
	    	drawLine(this.context,  x1, y1, x2, y2);
	    }
	    
	    var Grid = function(that, color){
	    	
	    	that.context.strokeStyle = color;
	    	
			for(var k = 1; k < that.BlockCols; k++){
				drawLine(that.context, that.BlockWidth * k, 0, that.BlockWidth * k, that.Height);
			} 

			for(var k = 1; k < that.BlockRows; k++){
				drawLine(that.context, 0, that.BlockHeight * k, that.Width, that.BlockHeight * k);
			}
	    	
	    }
	    
	    
	    var Clear = function(that, color){
	    	
	    	//context.clearRect(0,0,that.Width,that.Height);
	    	that.context.fillStyle = color;
	    	that.context.fillRect(0,0,that.Width,that.Height);
	    	
	    }

	    
	    /**
	     * @Description coloca um indice de um objeto solido em uma area, posiciona o objeto relativo a esta area
	     * */
	    Scene.prototype.addSolid = function(solidIndex, col, row, offset_x, offset_y, zoom_x, zoom_y, Cols, Rows){
	    	
	    	if(typeof that.SolidAreas[that.BlockCols * row + col] === "undefined"){
	    		that.SolidAreas[that.BlockCols * row + col] = new Array();
	    	}
	    	
	    	if(typeof zoom_x === "undefined"){
	    		zoom_x = 1;
	    		zoom_y = 1;    		
	    	}
	    	
	    	if(typeof Cols ===  "undefined"){
	    		Cols = 1;
	    		Rows = 1;    		
	    	}
	    	
	    	
	    	var areaIndex = that.BlockCols * row + col;

	    	
	    	
	    	var width = solid.objects[solidIndex].w * zoom_x;
			var height = solid.objects[solidIndex].h * zoom_y;
			
			var translate_x = 0;
			var translate_y = 0;
			//var current_height = 0;
	    	
	    	for(var i = 0; i < Cols; i++){
	    		
	    		translate_y = 0;
	    		for(var j = 0; j < Rows; j++){
	    			
	    			
	    			//TODO: criar um objeto
	    	    	//varios objetos podem ocupar a mesma area??
	    	    	var obj = {
	    	    			"solidIndex" : solidIndex,
	    	    			"x": that.BlockWidth * col + offset_x + translate_x,
	    					"y": that.BlockHeight * row + offset_y + translate_y,
	    					"w": solid.objects[solidIndex].w * zoom_x,
	    					"h": solid.objects[solidIndex].h * zoom_y,
	    					"col": col,
	    					"row": row
	    	    	};
	    	    	
	    	    	
	    	    	
	    	    	//Adiciona para mostrar
	    	    	definedSolids[definedSolids.length] = obj;
	    	    	
	    	    	//na coluna zero
	    			translate_y += height;
	    			
	    		}
	    		translate_x += width -1;
	    	}
	    	
	    	
	    	//Adiciona na area de colisao
	    	// é melhor vc imprimir tudo o que estiver na area?
	    	var area = that.SolidAreas[areaIndex];
	    	if(typeof area === "undefined"){
				area = new Array();
			}
	    	
	    	var len = area.length;
	    	
	    	var stroke = {
	 	    			"type": "obstacle", //TODO: rever se vai ficar desse jeito
	 	    			"solidIndex" : solidIndex,
	 	    			"x": that.BlockWidth * col + offset_x,
	 					"y": that.BlockHeight * row + offset_y,
	 					"x2": that.BlockWidth * col + offset_x + translate_x,
	 					"y2": that.BlockHeight * row + offset_y + translate_y,
	 					"cx": that.BlockWidth * col + offset_x + translate_x / 2,
	 					"cy": that.BlockHeight * row + offset_y + translate_y / 2,
	 					"w": translate_x,
	 					"h": translate_y    			
	    	};
	    	
	    	
	    	area[len] = stroke;
	    	
	    	//so por seguranca
	    	//that.SolidAreas[areaIndex] = area;
	    	
	    	var areasX = 1, areasY = 1;
	    	
	    	var axay = false;
	    	
	    	if(offset_x + stroke.w > that.BlockWidth){
	    		
	    		areasX = Math.ceil( (offset_x + stroke.w) / that.BlockWidth);
	    		//console.log("areasX",areasX);
	    		axay = true;
	    	}
	    	
	    	if(offset_y + stroke.h > that.BlockHeight){
	    		
	    		areasY = Math.ceil((offset_y + stroke.h)/ that.BlockHeight);
	    		//console.log("areasY",areasY);
	    		axay = true;
	    	}
	    	
	    	
	    	//Acrescentar o objeto nas ares que ocupa
	    	if(axay){	    	
		    	for(var i = 0; i < areasY; i++){
		    		//console.log("i->", i);
		    		//console.log("ay", areasY);
		    		
		    		for(var j = 0; j <= areasX; j++){
		    		
		    			
		    			//console.log("j-->", j);
		    			
		    			var newAreaIndex = areaIndex + that.BlockCols * i + j;
		    			var area2 = that.SolidAreas[newAreaIndex];
		    			
		    			if(typeof area2 === "undefined"){
		    				area2 = new Array();
		    				
		    			}
		    			area2[area2.length] = stroke;
		    			
		    			//adicionando??
		    			that.SolidAreas[newAreaIndex] = area2;
		    			
		    			
		    			//console.log("added", newAreaIndex);
		    			
		    		}
		    	}
		    	
		    	
	    	}//if axay
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    }
	    
	    
	    /**
	     * @Description funcao para montar a cena
	     */
	    Scene.prototype.makeScene = function(){
	    	
	    	/*
	    	var scale_x = 1, scale_y = 1;
	    	
	    	
	    	addSolid(2, 1, 3, 30, 17, scale_x, scale_y, 10, 10);
	    	
	    	addSolid(2, 2, 4, 45, 17, scale_x, scale_y, 7, 1);
	    	
	    	addSolid(2, 4, 3, 30, 17, scale_x, scale_y, 4, 1);
	    	
	    	
	    	addSolid(2, 6, 2, 30, 30, scale_x, scale_y, 4, 1);
	    	
	    	*/
	    }
	    
	    
	    var DrawSolids = function(that){
		    	
		    	if(that.readyState){
		    
		    		for(var i = 0; i < definedSolids.length; i++){
		    		var offset = definedSolids[i];
		    		
					that.context.drawImage(that.img,
							solid.objects[offset.solidIndex].x,
							solid.objects[offset.solidIndex].y,
							solid.objects[offset.solidIndex].w,
							solid.objects[offset.solidIndex].h,
							offset.x,
							offset.y,
							offset.w,
							offset.h);
					
		    		}
		    	
		    	} //end if Ready
		    	else{
		    		//Se a imagem não tiver sido carregada .. mostra o contorno
		    		for(var i = 0; i < that.SolidAreas.length; i++){
			    		var area = that.SolidAreas[i];
		    		
			    		if(typeof area !== "undefined"){
			    		
				    		for(var j = 0; j < area.length; j++){
				    			
				    			that.context.rect(
				    					area[j].x,
				    					area[j].y,
				    					area[j].w,
				    					area[j].h);
								that.context.stroke();
				    			
				    		}
			    		
			    		}
			    		
		    	
		    		}
	    	
	    	}
	    }
	    
	    Scene.prototype.clearMobs = function(){
		    while(mobs.length > 0){
				var len = mobs.length - 1;
	    		delete mobs[len];
	    		mobs.length = len;
	    	}
	    }
	    
	    
	  //saber em que posicao esta um ponto
	    Scene.prototype.getIndex = function(scene, _x, _y){
	    	
	    	var col = Math.floor(_x/scene.BlockWidth);
			var row = Math.floor(_y/scene.BlockHeight);
			
			return scene.BlockCols * row + col;
			
	    }
	    
	    /*
	     * Montar a cena aqui..
	     */
	    Scene.prototype.draw = function(delta){
	    	
	    	//*
	    	Clear(this, '#bbbbff');
	    	Grid(this, '#0000ff');
	    	//*/
	    	
	    	/*
	    	Clear('#bbbbff');
	    	Grid('#000000');
	    	//*/
	    	
	    	
	    	DrawSolids(this);
	    	
	    	//obs os objetos não serao chamados pelo nome, acho que não.
	    	
	    	if(player !== null)
	    		player.draw(delta);
	    	
	    	for(var i = 0; i < mobs.length; i++){
	    		mobs[i].draw(delta);
	    	}
	    	
	    	
	    }   

	    return Scene;

	  })();

	  window.Scene = Scene;

	}).call(this);
//end scene

//begin chacacter
(function () {
	  var Character;
	  
	  Character = (function() {
	    
		//TODO: definir as animações separadamente
		//TODO: uma animação para cada ação do personagem
	    function Character(ani) {
	    	var that = this;
	    	
	    	this.img = new Image();
	    	
	    	this.scene = null;
	    	
	    	/*
	    	 * 30 frames por segundo * segundo = 30
	    	 * 30/1000 * 10000 = 30;
	    	 * ou seja em 1000 eu vou ter 30;
	    	 * dado um tempo N dividir por 1000
	    	 * se esse numero for ou maior a 30/1000
	    	 * reduzir 30/1000 do numero
	    	 * */
	    	
	    	
	    	//this.delta = 0;
	    	this.fps = 1;
	    	
	    	this.delta = 0;
//	    	this.td = 0;
	    	
	    	
	    	
	    	this.animationIndex = 0;
	    	this.readyState = false;
	    	
	    	
	    	//variaveis auxiliares
	    	var ds;
	    	var dt;
	    	
	    	// n pixels por segundo ao quadrado
	    	ds = 300;
	    	dt = 1000; //1s
	    	this.ACCELERATIONY = ds / (dt * dt); //constante
	    	this.accelerationY = this.ACCELERATIONY; //variavel
	    	this.translateY = 0;
	    	this.velocityY = 0;
	    	
	    	this.VELOCITY_JUMP  = -300 / 1000;
	    	

	    	// n pixels por segundo
	    	ds = 80;
	    	dt = 1000;  //1s
	    	this.VELOCITYX_WALK = ds / dt; //constante
	    	
	    	ds = 110;
	    	dt = 1000;  //1s
	    	this.VELOCITYX_JUMP = ds / dt; //constante
	    	
	    	
	    	this.velocityX = this.VELOCITYX_WALK;
	    	this.translateX = 100;
	    	
	    	this.CHARACTER_COLISION_NOPE = false;
	    	this.CHARACTER_COLISION_TOP = false;
	    	this.CHARACTER_COLISION_BOTTOM = false;
	    	this.CHARACTER_COLISION_LEFT = false;
	    	this.CHARACTER_COLISION_RIGHT = false;
	    	this.CHARACTER_MOVE_RIGHT = false;
			this.CHARACTER_MOVE_LEFT = false;
			this.CHARACTER_MOVE_UP = false;
			this.CHARACTER_MOVE_DOWN = false;
			this.CHARACTER_OVERFLOW_GROUND = false;
			this.CHARACTER_OVERFLOW_LEFT = false;
			this.CHARACTER_OVERFLOW_RIGHT = false;
			this.CHARACTER_IS_DEAD = false;
			this.CHARACTER_READY_STATE = false;
			
			this.CURRENT_ANIMATION = "direita";
	    	
	    	//prototype
	    	this.animations = null;
	    	
	    	once(this.img,'load', function(){
	    		that.CHARACTER_READY_STATE = true;
	    	});
	    	
	    	//TODO: rever onde vai ficar isso
	    	//if(typeof (animation) !== "undefined"){
	    		
	    		
	    		//console.log(this.type);
	    		
	    		try{
		    		this.animations = ani;
					this.w = ani.w;
					this.h = ani.h;
					this.img.src = ani.original_name;
	    		}
	    		catch(e){
	    			//console.error(this.type,"exception");
	    		}

				
				
	    	//}
	    	
	    	
	    	//this.status = new Array();
	    	
	    	
	    }

	    Character.prototype.setContext = function(Context){
	    	context = Context;
	    }
	    
	    Character.prototype.setScene = function(Scene){
	    	tscene = Scene;
	    }
	    
	    Character.prototype.fitScreen = function(){
h
	/*	
	    	this.translateX *= scene.scaleX;
	    	this.translateY *= scene.scaleY;
	*/    	
	    	/*
	    	JUMPV = JUMPV0 * scene.scaleX;
	    	WALKV = WALKV0 * scene.scaleX;
	    	
	    	/*
	    	deltaX = vox0 * scene.scaleX;
	    	deltaY = voy0 * scene.scaleY;
	    	*/
	    }

	    
	    var pushUnique = function(arr, element, comparer){
	    	
	    	if(typeof comparer === "undefined"){
	    		for(var i = 0; i < arr.length; i++){	
		    		if(arr[i] === element)
		    			return;
		    	}
	    	}
	    	else{
		    	for(var i = 0; i < arr.length; i++){	
		    		if(comparer(arr[i]))
		    			return;
		    	}
	    	}
	    	
	    	arr[arr.length] = element;
	    }
	    
	    
	       /*        /  
	         \     /  
	           \ /  
	            /\  
	          /    \  
	        /        \*/
	    function colide(that, x, y, o, _cx, _cy, foot, right){
	    	
	    	
	    	if(foot){
	    		that.CHARACTER_COLISION_BOTTOM = false;
	    	}
	    	else{
	    		that.CHARACTER_COLISION_TOP = false;
	    	}
	    	
	    	if(right){
	    		that.CHARACTER_COLISION_RIGHT = false;
	    	}
	    	else{
	    		that.CHARACTER_COLISION_LEFT = false;
	    	}
	    	
	    	
	    	if ((x >= o.x) && (x <= o.x2) && (y >= o.y) && (y <= o.y2) ){
	    		
	    		if(y < toFNgetY(o, 0, _cx)){ //## e\
	    			//left bottom
	    			if(y > toFNgetY(o, 1, _cx)){ // ## e/
	    				//console.log("L < 0, > 1");
	    				that.CHARACTER_COLISION_RIGHT = true;
	    			}
	    			else{  // ## /d
	    				//console.log("T < 0, < 1");
	    				
	    				if(foot){ //identificando que o ponto esta no pe do personagem
	    					that.CHARACTER_COLISION_BOTTOM = true;
	    					//alert(true);
	    				}
	    				else{
	    					that.CHARACTER_COLISION_TOP = true;
	    				}
	    			}
	    		}
	    		else{  //## \d
	    			//top right
	    			if(y > toFNgetY(o, 1, _cx)){  // ## e/
	    				//console.log("B > 0, > 1");
	    				that.CHARACTER_COLISION_TOP = true;
	    			}
	    			else{  // ## /d
	    				//console.log("R > 0, < 1");
	    				that.CHARACTER_COLISION_LEFT = true;
	    			}
	    		}
	    		
	    		return true;
	    	}
	    	
	    	return false;
	    }
	    
	    var toFNgetY = function(o, index, x){
	    	
	    	if(typeof index === "undefined")
	    		index = 0;

	    	var a, b;
	    	if(index == 0){
	    		a = o.h / o.w * -1;
	    		b = o.y - a * o.x + o.h;
	    	}
	    	else{
	    		a =  o.h / o.w;    		
	    		b = o.y - a * o.x;
	    	}
	    	
	    	return a * x + b;
	    	
	    }
	    
	    Character.prototype.draw = function(delta) {
	    	
	    	if(this.CHARACTER_IS_DEAD){ 
	    		return;
	    	}
	    		    	
	    	//Usado somente nos personagens automaticos
	    	if(typeof this.updateMotion == "function")
	    		this.updateMotion(delta);
	    	
	    	var o0 = this.animations.animations[this.CURRENT_ANIMATION];
	    	
	    	//console.log(this.animations.animations[this.CURRENT_ANIMATION]);
	    	
			var o1 = o0.objects[0];
			
			this.w = o1.w;
			this.h = o1.h;
			
			
			if(typeof this.w === "undefined" || this.w == null){
				
				console.log(o1);
				
				throw "zzz";
				
			}
			
			
			
			var corners = new Array();
			
			corners[0] = this.scene.getIndex(this.scene, this.translateX, this.translateY);
			corners[1] = this.scene.getIndex(this.scene, this.translateX+this.w, this.translateY);
			corners[2] = this.scene.getIndex(this.scene, this.translateX, this.translateY+this.h);
			corners[3] = this.scene.getIndex(this.scene, this.translateX+this.w, this.translateY+this.h);
			
			for(var i = 0; i < corners.length; i++){
				var objects = this.scene.SolidAreas[corners[i]];
				if(typeof objects !== "undefined"){
									
					for(var j = 0; j < objects.length; j++){
						//o x usado para verificar o lado da colisao;
						//x do meio do objeto
						var _cx = this.translateX + this.w / 2 * this.scene.scaleX;
						var _cy = this.translateY + this.h / 2 * this.scene.scaleY;
						
						colide(this,
						this.translateX + (this.w * this.scene.scaleX),
						this.translateY + (this.h * this.scene.scaleY),
						objects[j],
						_cx,
						_cy,
						true,
						true);
						
						colide(this,
						this.translateX,
						this.translateY + (this.h * this.scene.scaleY),
						objects[j],
						_cx,
						_cy,
						true,
						false);
								
						colide(this,
						this.translateX + (this.w * this.scene.scaleX),
						this.translateY,
						objects[j],
						_cx,
						_cy,
						false,
						true);
								
						colide(this,
						this.translateX,
						this.translateY,
						objects[j],
						_cx,
						_cy,
						false,
						false);
						
					}//for objects
				}//if objects defined
			}//for each corner
			
			
//			delete corners[0];
//			delete corners[1];
//			delete corners[2];
//			delete corners[3];
			
//######################################################## movimentação: novo

			
			
	    	//Ação da Gravidade
	    	if(!this.CHARACTER_IS_DEAD && (this.CHARACTER_COLISION_BOTTOM || (this.translateY + this.h >= 6 * this.scene.BlockHeight))){
	    		//A velocidade de caida e constante igual a zero;
	    		this.velocityY = 0;
	    		//this.velocityX = this.VELOCITX_WALK;
	    		
		    	//Pula somente se estiver colidindo no chão
		    	if(this.CHARACTER_MOVE_UP){
		    		this.velocityX = this.VELOCITYX_JUMP;
		    		this.velocityY = this.VELOCITY_JUMP;
		    		this.translateY = this.translateY + this.velocityY * delta + this.accelerationY * delta * delta / 2; //falta ajustar o zoom..
		    		this.velocityY = this.velocityY + this.accelerationY * delta; //velocidade variavel;
				}
	    		
	    	}
	    	else{
//	    		//A velocidade de caida vai aumentando conforme o tempo;
	    		//this.translateY = this.translateY + this.velocityY * delta; //falta ajustar o zoom..
	    		this.translateY = this.translateY + this.velocityY * delta + this.accelerationY * delta * delta / 2;
		    	this.velocityY = this.velocityY + this.accelerationY * delta; //velocidade variavel;
	    	}
	    	
	    	//se abaixa
	    	if(this.CHARACTER_COLISION_BOTTOM && this.CHARACTER_MOVE_DOWN){
	    		
			}
	    	
	    	//move para a DIREITA somente se não estiver movendo para a esquerda
	    	if(this.CHARACTER_MOVE_RIGHT){
	    		//console.log("this.CHARACTER_MOVE_RIGHT", this.CHARACTER_MOVE_RIGHT);
	    		this.translateX += this.velocityX * delta;
	    		//TODO: falta ajustar o zoom..
	    		//TODO: Falta atualizar a animação
	    		//console.log(">>>", this.translateX);
			}
	    	
	    	//move para a ESQUERDA somente se não estiver movendo para a direita
			if(this.CHARACTER_MOVE_LEFT){
				this.translateX -= this.velocityX * delta;
				//TODO: falta ajustar o zoom..
				//TODO: Falta atualizar a animação
				//console.log("<<<", this.translateX);
			}
			
////######################################################## movimentação: novo

				
				//desenhar o proximo frame
				if(this.CHARACTER_READY_STATE){
					
					//console.log("this.translateX", this.translateX);
					//console.log("this.CHARACTER_MOVE_RIGHT", this.CHARACTER_MOVE_RIGHT);
					//console.log("this.CHARACTER_MOVE_LEFT", this.CHARACTER_MOVE_LEFT);
					
					if(this.CHARACTER_MOVE_RIGHT || this.CHARACTER_MOVE_LEFT){
						
						//this.delta += delta;
						
						o1 = o0.objects[this.animationIndex];
						
						if(typeof(o1) === "undefined"){
							console.log(o0);
							console.log(o1.x);
							throw "undefined variable";
						}
						
						
						this.scene.context.drawImage(
							this.img,
							o1.x,
							o1.y,
							o1.w,
							o1.h,
							this.translateX,
							this.translateY,
							o1.w * this.scene.scaleX,
							o1.h * this.scene.scaleY
						);
						
//						console.log(delta);
												
						this.delta += delta;
						
//						if(this.delta > 1000){
//							throw "1000";
//						}
						
						
						if(this.delta > this.fps){
							
							this.delta = this.delta - this.fps;
							
							
							this.animationIndex++;
							if(this.animationIndex === o0.objects.length){ this.animationIndex = 0; }
							//console.log("MOVENDO", this.animationIndex);
							//console.log(this.animationIndex);
						}
					}
					else{
						this.scene.context.drawImage(
							this.img,
							o1.x,
							o1.y,
							o1.w,
							o1.h,
							this.translateX,
							this.translateY,
							o1.w * this.scene.scaleX,
							o1.h * this.scene.scaleY
						);
					}
				}
				else{
					this.scene.context.rect(
						this.translateX,
						this.translateY,
						o0.w * this.scene.scaleX,
						o0.h * this.scene.scaleY
					);
					this.scene.context.stroke();	
				}
		
//				//gravidade
//				if(this.translateY + deltaY + o0.h <= 6 * this.scene.BlockHeight){ //this.translateY = this.translateY + 7 * 1.1* 1.1 / 2; 
//					this.translateY = this.translateY + deltaY;
//					
//				}
//				else{
//					//alert("you loose");
//					//this.translateY = this.translateY + deltaY;
//					deltaX = WALKV;
//					ground = true;
//				}
//				
//				//morte por queda
//				if(!this.CHARACTER_IS_DEAD && this.translateY  > o0.h + 6 * this.scene.BlockHeight){ //this.translateY = this.translateY + 7 * 1.1* 1.1 / 2; 
//					this.CHARACTER_IS_DEAD = true;
//					alert("you loose");
//				}
//				
//				
//				//morte ao sair da tela..
//				if(!this.CHARACTER_IS_DEAD && this.translateX  > o0.w + this.scene.context * scene.BlockHeight){ //this.translateY = this.translateY + 7 * 1.1* 1.1 / 2; 
//					this.CHARACTER_IS_DEAD = true;
//					alert("you loose");
//				}
				

	    };
	    

	    return Character;

	  })();

	  window.Character = Character;
	  
}).call(this);
//end end character


//window.Character = AbstractCharacter();


//begin player
(function() {
	  var Player;
	  
	  Player = (function(_super) {
	    __extends(Player, _super);
	    
		var	KEYS_SPACE = 32,
		KEYS_Q = 81,
		KEYS_W = 87,
		KEYS_E = 69,
		KEYS_A = 65,
		KEYS_S = 83,
		KEYS_D = 68,
		KEYS_LEFT = 37,
		KEYS_UP = 38,
		KEYS_RIGHT = 39,
		KEYS_DOWN = 40,
		KEYS_CONTROL = 17;
	    
	    
	    function Player() {
	    	
	    	Player.__super__.constructor.apply(this, arguments);
	    	var that = this;
	    	
	    	this.translateY = 200;
	    	this.translateX = 10;
	    	
	    	this.keys = [];
	    	once(document, "keydown", function (e) {
	    		switch (e.keyCode) {
	    			case KEYS_SPACE:
	    				that.CHARACTER_MOVE_UP = true;
	    				break;
	    			case KEYS_Q:
	    			case KEYS_W:
	    			case KEYS_E:
	    			case KEYS_A:
	    			case KEYS_S:
	    			case KEYS_D:
	    			case KEYS_LEFT:
	    				that.CHARACTER_MOVE_LEFT = true;
	    				break;
	    			case KEYS_UP:
	    			case KEYS_RIGHT:
	    				that.CHARACTER_MOVE_RIGHT = true;
	    				break;
	    			case KEYS_DOWN:
	    			case KEYS_CONTROL:
	    			//thisPlayer.keys[e.keyCode] = e.keyCode;
	    		}
	    		return false;
	    	});

	    	once(document, "keyup", function (e) {
	    		
	    		switch (e.keyCode) {
	    		case KEYS_SPACE:
	    			that.CHARACTER_MOVE_UP = false;
					break;
				case KEYS_Q:
				case KEYS_W:
				case KEYS_E:
				case KEYS_A:
				case KEYS_S:
				case KEYS_D:
				case KEYS_LEFT:
					that.CHARACTER_MOVE_LEFT = false;
					break;
				case KEYS_UP:
				case KEYS_RIGHT:
					that.CHARACTER_MOVE_RIGHT = false;
					break;
				case KEYS_DOWN:
				case KEYS_CONTROL:
	    			//thisPlayer.keys[e.keyCode] = false;
	    		}
	    		return false;
	    	});
	    	
	      return ;
	    }

	    Player.prototype.type = "player";
	    
	    return Player;

	  })(Character);
	  
	  window.Player = Player;
	  
}).call(this);
//end end player

//begin end Mob
(function() {
	  var Mob;
	  Mob = (function(_super) {
	    __extends(Mob, _super);
	    var that;
	    
	    //o movimento do mob muda toda vez que ele
	    //sair da area de movimentacao
	    
	    //TODO: como colocar um mob em uma area
	    //TODO: como mudar o mob de area
	    function Mob(arg0, arg1) {
	      Mob.__super__.constructor.apply(this, arguments);
	      that = this;
	      
	      this.fps = 400;
	      
	      this.MOB_LEFT = 10;
    	  this.MOB_RIGHT = 150;
    	  
    	  this.FLOOR = false;
	      
    	  
    	  this.delimite = function(col1, row, limitX, distance, translateX, translateY, toRight){    		  
    		  
    		  this.MOB_LEFT = this.scene.BlockWidth *  col1 + limitX;
        	  this.MOB_RIGHT = this.MOB_LEFT + distance;

        	  that.CHARACTER_MOVE_UP = true;
        	  
        	  this.translateY = this.scene.BlockHeight * row + translateY;
        	  if(toRight){
        		    this.translateX = this.MOB_LEFT + translateX;
        		    this.CHARACTER_MOVE_RIGHT = true;
        		    this.CHARACTER_MOVE_LEFT = false;
        		    this.CURRENT_ANIMATION = "direita";
        		    this.animationIndex = 0;
	      	  }
	      	  else{
	      		    this.translateX = this.MOB_RIGHT - translateX;
	      		    this.CHARACTER_MOVE_RIGHT = false;
	      		    this.CHARACTER_MOVE_LEFT = true;
	      		    this.CURRENT_ANIMATION = "esquerda";
	      		    this.animationIndex = 0;
	      	  }
    		  
    	  }
	      
	      this.updateMotion = function(delta){
	    	  
	    	  //scene.MobAreas
	    	  
	    	  //colocar o mob em uma area ou retirar o mob de uma area
	    	  
	    	  
				this.scene.drawLine('#eeaa00', this.MOB_LEFT, 0 , this.MOB_LEFT, this.scene.Height);
				this.scene.drawLine('#eeaa00', this.MOB_RIGHT, 0 , this.MOB_RIGHT, this.scene.Height);
	      		      		
				
//				if(this.CHARACTER_COLISION_BOTTOM){
//					this.FLOOR = true;
//				}
//				
//				if(this.FLOOR){
//					this.CHARACTER_COLISION_BOTTOM = true;
//				}
				
				
	      		if(this.translateX + this.w > this.MOB_RIGHT){
	      			this.CHARACTER_MOVE_RIGHT = false;
	      			this.CHARACTER_MOVE_LEFT = true;
	      			this.CURRENT_ANIMATION = "esquerda";
	      			
	      		}
	      		
	      		if(this.translateX < this.MOB_LEFT){
	      			this.CHARACTER_MOVE_RIGHT = true;
	      			this.CHARACTER_MOVE_LEFT = false;
	      			this.CURRENT_ANIMATION = "direita";
	      		}
	    	  		
	      		
	      		
	      }
	      
	    }

		Mob.prototype.type = "Mob";

	    return Mob;

	  })(Character);
	  //TODO: talvez eu tenha o mesmo problema quando
	  // com a variavel that
	  
	  window.Mob = Mob;

}).call(this);


//end chacacter-and-player-and-Mob