#!/usr/local/bin/node

/*
* @author: uisleandro
* @email: uisleandro@gmail.com
*/

'use strict';

var anim = "mob001.anim", sprites = "mob001.sprites", out = "mob001.json";

for(var i = 0; i < process.argv.length; i++){
	if(process.argv[i].substring(0,2) === "-a"){
		anim = process.argv[i+1];
	}

	if(process.argv[i].substring(0,2) === "-s"){
		sprites = process.argv[i+1];
	}

	if(process.argv[i].substring(0,2) === "-o"){
		out = process.argv[i+1]
	}
}

console.log("anim", anim);
console.log("sprites", sprites);
console.log("out", out);


var fs = require('fs'),
    xml2js = require('xml2js'),
    async = require('async');

var parser = new xml2js.Parser();

async.parallel([
	function(callback){
	fs.readFile(__dirname + '/'+sprites,
		function(err, data) {
			parser.parseString(data,
			function (err, result) {
				//console.log("proc "+sprites);

				//                                    root/firstChild

//console.log("result.img.definitions[0].dir[0].spr[0]");
//console.log(result.img.definitions[0].dir[0].spr[0]);

				var spr = result.img.definitions[0].dir[0].spr;

				var original_name = result.img.$.name;
				var original_width = result.img.$.w; 
				var original_height = result.img.$.h;

//				var maxW = 0;
//				var maxH = 0;

				var objects = [];
				var j = 0;

				for(var i = 0; i < spr.length; i++){

//
//					if(spr[i].$.w > maxW){
//						maxW = spr[i].$.w;
//					}
//
//					if(spr[i].$.h > maxH){
//						maxH = spr[i].$.h;
//					}
//

					objects[j++] = spr[i].$;
				} //for

				callback(null, {
				"type": "image",
				"name": original_name,
				"width": parseInt(original_width),
				"height": parseInt(original_height),
//				"frame_count": spr.length,
//				"frame_width": parseInt(maxW),
//				"frame_height": parseInt(maxH),
				"objects": objects
				}); //callback
			}); //parseString
		}) //readFile
	},
	function(callback){
		fs.readFile(__dirname + '/' + anim,
		function(err, data) {
			parser.parseString(data,
			function (err, result) {
				var spriteSheet = result.animations.spriteSheet;


var animations = [];

//for each animation
for (var z = 0; z < result.animations.anim.length; z++){


				var info = result.animations.anim[z].$;
				var spr = result.animations.anim[z].cell;


//console.log("anim "+ JSON.stringify(info));
//console.log("proc "+ anim);

//console.log("spr is an array? "+ spr);
	
				var index = 0, delay = 0, objects = [], j = 0;

				for(var i = 0; i < spr.length; i++){

					index = spr[i].$.index;
					delay = spr[i].$.delay;

					var obj = spr[i].spr[0].$;
					obj.index = index;
					obj.delay = delay;
					obj.fk = obj.name.substring(obj.name.lastIndexOf('/')+1);

					objects[j++] = obj;

					//console.log(obj);
					//console.log(spr[i].spr[0].$);

				} //for


				var anim = {
				"name": info.name,
				"loops": parseInt(info.loops),
				"objects":objects
				}

				//console.log("anim",anim);


				animations[animations.length] = anim;

} //for each animation

				//falta corrigir aqui
				callback(null, {"type": "animations", "value": animations } ); //callback



			}); //parseString
		}) //readFile
	}],
	function(err, results){

		var img, anims;
		var k = 0;

		if(results[0].type === "image")
		{
			img = results[0]; 		
			anims = results[1].value;
		}else{
			img = results[1]; 		
			anims = results[0].value;
		}

var Animations = {};		

for(var z = 0; z < anims.length; z++){ //for each animation

var anim = anims[z];
var objects = [];


		var maxW = 0;
		var maxH = 0;

		//para cada objeto na animacao
		for(var i = 0; i < anim.objects.length; i++){
			for(var j = 0; j < img.objects.length; j++){
				if(img.objects[j].name === anim.objects[i].fk){

					// combine anim and img
					//console.log("maxH?", img.objects[j]);

					var w = parseInt(img.objects[j].w);
					var h = parseInt(img.objects[j].h);

					if(w > maxW){
						maxW = w
					}

					if(h > maxH){
						maxH = h;
					}

					objects[k++] = {
						id : parseInt(anim.objects[i].index),
						delay : parseInt(anim.objects[i].delay),
						x : parseInt(img.objects[j].x),
						y : parseInt(img.objects[j].y),
						w : parseInt(img.objects[j].w),
						h : parseInt(img.objects[j].h),
						gx : parseInt(anim.objects[i].x),
						gy : parseInt(anim.objects[i].y),
						gz : parseInt(anim.objects[i].z)
					}
					//novo
					if(anim.objects[i].flipH){
						objects[k-1].flipH = 1;
					}
					//novo
					if(anim.objects[i].flipV){
						objects[k-1].flipV = 1;
					}
				}//end if --- vericicaçao da chave estrangeira
			}//end for -- foreach object into img
		}//end for  -- for each object into anim


Animations[anim.name] = {
"loops" : anim.loops,
"length": objects.length,
"w": maxW,
"h": maxH,
"objects": objects
};

k = 0;

} //for each animation

		//write out to file
		var contents = JSON.stringify({
			"type": "animation-coords",
			"src": img.name,
			"width": img.width,
			"height": img.height,
//			"frame_count": img.frame_count,
//			"w":img.frame_width,
//			"h":img.frame_height,
			"animation":Animations});

		contents = contents.replace(/\}\,\{/g,"},\n\t\t{").replace(/\[\{/g,"[\n\t\t{").replace(/:\{/g,":\n\t{").
replace(/\}\]\}\,/g,"}]},\n\t");


		fs.writeFile(out, contents, function(err) {
		    if(err) {
			console.log(err);
		    } else {
			console.log("SUCCESSiFULLY FINISHED");
		    }
		}); //writeFile
	}
); //end assync parallel

