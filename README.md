game-product-line
=================

Product line for a 2d game development


Click in the following picture to open a video about how to import this project on your http://c9.io/ workspace:
[![ScreenShot](https://i.vimeocdn.com/video/509095123.jpg?mw=960&mh=540)](https://vimeo.com/121042773)


To edit this project on your computer, you will need:

	http://nodejs.org/download/
	
	https://eclipse.org/downloads/

	http://www.nodeclipse.org/

	http://darkfunction.com/editor/

if you want to edit the diagrams:

	http://wiki.eclipse.org/EMF_Feature_Model/Feature_Diagram_Editor
	
to choose the features to generate:

	edit the file gen.xml
	
to generate a new product run the following command:

	node gen.js
	or else
	node gen
	
to use different pictures and animations:

	find some tutorials of the darkfunction editor on youtube
	

to alter the generated code please go to the directory:

	public/js
	
	make all modifications you need then generate the code

to make different scenes please see:

	Scene.prototype.makeScene = function(){
	...
	}
	
	
to run this project call:

	node app.js
	or else
	node app
	
limitations:

	the colision system is not good seems
	the generated controls for android seems also not good
	