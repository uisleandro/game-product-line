"use strict";

(function() {
	  var Graphics;
	  Graphics = (function() {
			var that;
			function Graphics(CanvasId) {
				//
				this.context = null;
			}
			return Graphics;
			
			Graphics.prototype.drawLine = function(x1, y1, x2, y2){
				this.context.beginPath();
				this.context.moveTo(x1,y1);
				this.context.lineTo(x2, y2);
				this.context.stroke();
			}
			
			Graphics.prototype.Grid = function(cols, rows, width, height, color){
				
				if(typeof(color) !== "undefined")
					this.context.strokeStyle = color;
				else
					this.context.strokeStyle = "#000000";
				
				for(var k = 1; k < cols; k++){
					drawLine(this.context, width * k, 0, width * k, height);
				} 

				for(var k = 1; k < rows; k++){
					drawLine(this.context, 0, height * k, width, height * k);
				}
				
			}
			
			Graphics.prototype.Clear = function(that, color){
				
				//context.clearRect(0,0,that.Width,that.Height);
				this.context.fillStyle = color;
				this.context.fillRect(0,0,that.Width,that.Height);
				
			}
			
			
	  });
	  
	  
	  
	  
	  return Graphics;
	    
}).call(this);