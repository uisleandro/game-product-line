"use strict";

var once=function(element,evento,funcao){
	if(element.addEventListener){
		return element.addEventListener(evento, funcao, false);
	}
	else{
		return element.attachEvent("on"+evento,funcao);
	}
};

var __hasProp = {}.hasOwnProperty,
__extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };



var drawLine = function(context, color, x1, y1, x2, y2){
	context.beginPath();
	context.moveTo(x1,y1);
	context.lineTo(x2, y2);
	context.stroke();
}


var Clear = function(that, color){
	
	//context.clearRect(0,0,that.Width,that.Height);
	that.context.fillStyle = color;
	that.context.fillRect(0,0,that.Width,that.Height);
	
}

var Grid = function(that, color){
	
	that.context.strokeStyle = color;
	
	for(var k = 1; k < that.BlockCols; k++){
		drawLine(that.context, that.BlockWidth * k, 0, that.BlockWidth * k, that.Height);
	} 

	for(var k = 1; k < that.BlockRows; k++){
		drawLine(that.context, 0, that.BlockHeight * k, that.Width, that.BlockHeight * k);
	}
	
}

var Circle = function(context, centerX, centerY, radius, fillCollor, strokeCollor, StrokeSize){
	
	
	if(typeof fillCollor === "undefined"){
		fillCollor = "rgba(0, 51, 0, 0.5)";
	}
	
	if (typeof strokeCollor === "undefined"){
		strokeCollor = "rgba(0, 0, 0, 0.5)";
	}
	
	if (typeof StrokeSize === "undefined"){
		StrokeSize = 1;
	}

	context.beginPath();
	context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
	context.fillStyle = fillCollor;
	context.fill();
	context.lineWidth = StrokeSize;
	context.strokeStyle = strokeCollor;
	context.stroke();
	
}


var isPointIncircle = function(px, py, cx, cy, radius){
	
	//h² = a² + b²
	var Dx = px - cx;
	var Dy = py - cy;
	var a2 = Math.pow(Dx, 2);
	var b2 = Math.pow(Dy, 2);
	var h2 = a2 + b2;
	var h = Math.sqrt(h2);
	
	return (h <= radius);
	
}

var getRadius = function(px, py, cx, cy){
	
	//h² = a² + b²
	var Dx = px - cx;
	var Dy = py - cy;
	var a2 = Math.pow(Dx, 2);
	var b2 = Math.pow(Dy, 2);
	var h2 = a2 + b2;
	var h = Math.sqrt(h2);
	
	return h;
	
}

var pushUnique = function(arr, element, comparer){
	
	if(typeof comparer === "undefined"){
		for(var i = 0; i < arr.length; i++){	
    		if(arr[i] === element)
    			return;
    	}
	}
	else{
    	for(var i = 0; i < arr.length; i++){	
    		if(comparer(arr[i]))
    			return;
    	}
	}
	
	arr[arr.length] = element;
}


/**
 * calcula uma funcao a partir de uma caixa
 * retorna o y da funcao
 * index : direcao da funcao o para descendente "\" e 1 para ascendente "/"
 * x : x da funcao
 * o : objeto do qual vai sair a funcao
 * retorna o y da função
 * @param object o
 * @param int index
 * @param number x
 * @return int
 * */
var toFNgetY = function(o, index, x){
	
	if(typeof index === "undefined")
		index = 0;

	//encontra a e b
	var a, b;
	if(index == 0){
		a = o.h / o.w * -1;
		b = o.y - a * o.x + o.h;
	}
	else{
		a =  o.h / o.w;    		
		b = o.y - a * o.x;
	}
	
	// angulo * x + b (funcao de x)
	return a * x + b;
	
}


var mobilecheck = function() {
	  var check = false;
	  (function(a,b){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
	  return check;
}





