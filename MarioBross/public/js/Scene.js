'use strict';

(function() {
	  var Scene;

	  Scene = (function() {
	    var that;
	    
	    function Scene(game) {
	    	that = this;
	    	
	    	this.player = null;
	    	
	    	this.game = game;
	    	
	    	this.definedSolids = new Array();
//###<% if(coins){ %>
	    	this.definedCoins = new Array();
//###<% } %>
	    	
	    	this.outplace = null;
	    	
	    	this.mobs = new Array();
	    		    	
	    	this.coords = {"type":"animation-coords","src":"img/solids1.png","width":662,"height":40,"animation":
	    	{"coin":
	    	{"loops":0,"length":4,"w":12,"h":16,
	    		"delay": 150,
	    		"objects":[
	    		{"id":0,"delay":5,"x":158,"y":10,"w":12,"h":16,"gx":0,"gy":0,"gz":0},
	    		{"id":1,"delay":5,"x":180,"y":10,"w":8,"h":16,"gx":0,"gy":0,"gz":0},
	    		{"id":2,"delay":5,"x":198,"y":10,"w":6,"h":16,"gx":0,"gy":0,"gz":0},
	    		{"id":3,"delay":5,"x":214,"y":10,"w":8,"h":16,"gx":0,"gy":0,"gz":0}]},
	    	"question":
	    	{"loops":0,"length":3,"w":16,"h":16,
	    		"delay": 160,
	    		"objects":[
	    		{"id":0,"delay":5,"x":258,"y":10,"w":16,"h":16,"gx":0,"gy":0,"gz":0},
	    		{"id":1,"delay":5,"x":284,"y":10,"w":16,"h":16,"gx":0,"gy":0,"gz":0},
	    		{"id":2,"delay":5,"x":310,"y":10,"w":16,"h":16,"gx":0,"gy":0,"gz":0}]},
	    	"music":
	    	{"loops":0,"length":3,"w":16,"h":16,
	    		"delay": 30,
	    		"objects":[
	    		{"id":0,"delay":5,"x":336,"y":10,"w":16,"h":16,"gx":0,"gy":0,"gz":0},
	    		{"id":1,"delay":5,"x":362,"y":10,"w":16,"h":16,"gx":0,"gy":0,"gz":0},
	    		{"id":2,"delay":5,"x":388,"y":10,"w":16,"h":16,"gx":0,"gy":0,"gz":0}]},
	    	"floor1":
	    	{"loops":0,"length":1,"w":16,"h":16,
	    		"delay": 30,
	    		"objects":[
	    		{"id":0,"delay":5,"x":232,"y":10,"w":16,"h":16,"gx":0,"gy":0,"gz":0}]},
	    	"door":
	    	{"loops":0,"length":1,"w":16,"h":33,
	    		"delay": 30,
	    		"objects":[
	    		{"id":0,"delay":1,"x":624,"y":0,"w":16,"h":33,"gx":0,"gy":-3,"gz":0}]},
	    	"out":
	    	{"loops":0,"length":1,"w":32,"h":24,
	    		"delay": 30,
	    		"objects":[
	    		{"id":0,"delay":1,"x":492,"y":10,"w":32,"h":24,"gx":1,"gy":-2,"gz":0}]},
	    	"coin2":
	    	{"loops":0,"length":2,"w":16,"h":25,
	    		"delay": 30,
	    		"objects":[
	    		{"id":0,"delay":10,"x":466,"y":10,"w":16,"h":25,"gx":0,"gy":-1,"gz":0},
	    		{"id":1,"delay":10,"x":440,"y":10,"w":16,"h":25,"gx":0,"gy":-1,"gz":0}]},
	    	"exclamation":
	    	{"loops":0,"length":1,"w":16,"h":16,
	    		"delay": 30,
	    		"objects":[
	    		{"id":0,"delay":1,"x":414,"y":10,"w":16,"h":16,"gx":0,"gy":0,"gz":0}]},
	    	"floor2":
	    	{"loops":0,"length":4,"w":16,"h":16,
	    		"delay": 30,
	    		"objects":[
	    		{"id":0,"delay":5,"x":534,"y":10,"w":16,"h":16,"gx":0,"gy":0,"gz":0},
	    		{"id":1,"delay":5,"x":560,"y":30,"w":16,"h":10,"gx":0,"gy":0,"gz":0},
	    		{"id":2,"delay":5,"x":10,"y":31,"w":16,"h":4,"gx":0,"gy":0,"gz":0},
	    		{"id":3,"delay":5,"x":560,"y":10,"w":16,"h":10,"gx":0,"gy":0,"gz":0}]}}}

	    	this.context = null;
	    	//this.canvas = null;
	    	
	    	this.readyState = false;
	    	this.img = new Image();
	    	
	    	once(that.img,'load', function(){
	    		that.readyState = true;
	    	});
	    	
	    	
	    	that.img.src = this.coords.src;
	    	
	    	this.BlockCols = 10;
	    	this.BlockRows = 6;
	    	
	    	
	    	this.SolidAreas = new Array(this.BlockCols * this.BlockRows);
	    	this.PlayerAreas = new Array(this.BlockCols * this.BlockRows);
	    	
	    	this.Width = 16*60;
	    	this.Height = 9*60;
	    	
	    	this.scaleX = 1;
	    	this.scaleY = 1;
	    	
	    	this.BlockWidth = this.Width / this.BlockCols;
	    	this.BlockHeight = this.Height / this.BlockRows;
	    	
	    	this.addPlayer();
	    }    
	    
	    Scene.prototype.addPlayer = function(){
	    	//para que seja possivel colocar os controles
	    	var player = new Player();
	    	this.game.player = player;
	    	player.scene = this;
	    	this.player = player;
	    }
	    
	    /*
	     * o mob atualiza sua posição entrando numa àrea
	     * o player verifica se está colidindo dentro da sua própria área
	     * */
	    
	    /**
	     * @Param Character character
	     * */
	    Scene.prototype.addMob = function(col1, row, limitX, distance, translateX, translateY, toRight, jump){
	    	
			var mob = new Mob();
	    	
	    	mob.scene = this;
	    	mob.delimite(col1, row, limitX, distance, translateX, translateY, toRight);
	    	
	    	if(jump){
	    		mob.CHARACTER_MOVE_UP = true;
	    	}
	    	
	    	
	    	this.mobs[this.mobs.length] = mob;
	    	
	    	
	    }
	    
//###<% if(zoom){ %>
	    Scene.prototype.fitScreen = function(){
	    	
	    	this.SolidAreas.length = 0;
	    	this.definedSolids.length = 0;
	    	
	    	this.Width = 16*60;
	    	this.Height = 9*60;
	    	
	    	this.scaleX = window.innerWidth / this.Width;
	    	this.scaleY = window.innerHeight / this.Height;
	    	
	    	this.BlockWidth = this.Width / this.BlockCols * this.scaleX;
	    	this.BlockHeight = this.Height / this.BlockRows * this.scaleY;
	    	
	    	this.Width = window.innerWidth;
	    	this.Height = window.innerHeight;
	    	
	    	if(this.player)
	    		this.player.fitScreen();
	    	
	    	for(var i = 0; i < this.mobs.length; i++){
	    		this.mobs[i].fitScreen();
	    	}

	    }
//###<% } %>
	    	    
		 /**
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
	     * @Description coloca um indice de um objeto solido em uma area, posiciona o objeto relativo a esta area
	     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
	     * */
	    Scene.prototype.addSolid = function(name, col, row, offset_x, offset_y, zoom_x, zoom_y, Cols, Rows){

	    	if(typeof that.SolidAreas[that.BlockCols * row + col] === "undefined"){
	    		that.SolidAreas[that.BlockCols * row + col] = new Array();
	    	}
	    	
	    	if(typeof zoom_x === "undefined"){
	    		zoom_x = 1;
	    		zoom_y = 1;    		
	    	}
	    	
	    	if(typeof Cols ===  "undefined"){
	    		Cols = 1;
	    		Rows = 1;    		
	    	}
	    	
	    	
	    	var areaIndex = that.BlockCols * row + col;

	    	//console.log(this.coords);
	    	
	    	var width = this.coords.animation[name].w * zoom_x;
			var height = this.coords.animation[name].h * zoom_y;
			
			var translate_x = 0;
			var translate_y = 0;
			//var current_height = 0;
	    	
	    	for(var i = 0; i < Cols; i++){
	    		
	    		translate_y = 0;
	    		for(var j = 0; j < Rows; j++){
	    			//TODO: criar um objeto
	    	    	//varios objetos podem ocupar a mesma area??
	    	    	var obj = {
	    	    			"type": "solid",
	    	    			"name" : name,
	    	    			"index": 0,
	    	    			"delta": 0,
	    	    			"x": that.BlockWidth * col + offset_x + translate_x,
	    					"y": that.BlockHeight * row + offset_y + translate_y,
	    					"w": this.coords.animation[name].w * zoom_x,
	    					"h": this.coords.animation[name].h * zoom_y,
	    					"col": col,
	    					"row": row
	    	    	};
	    	    	

	    	    	
	    	    	//Adiciona para mostrar
	    	    	this.definedSolids[this.definedSolids.length] = obj;
	    	    	
	    	    	//na coluna zero
	    			translate_y += height;
	    			
	    		}
	    		translate_x += width -1;
	    	}
	    	
	    	var stroke = {
	 	    			"type": "solid", //TODO: rever se vai ficar desse jeito
	 	    			"name" : name,
	 	    			"x": that.BlockWidth * col + offset_x,
	 					"y": that.BlockHeight * row + offset_y,
	 					"x2": that.BlockWidth * col + offset_x + translate_x,
	 					"y2": that.BlockHeight * row + offset_y + translate_y,
	 					"cx": that.BlockWidth * col + offset_x + translate_x / 2,
	 					"cy": that.BlockHeight * row + offset_y + translate_y / 2,
	 					"w": translate_x,
	 					"h": translate_y    			
	    	};
	    	
	    		    	
	    	var areasX = 1, areasY = 1;
	    	
	    	if(offset_x + stroke.w > that.BlockWidth){	    		
	    		areasX = Math.ceil( (offset_x + stroke.w) / that.BlockWidth);
	    	}
	    	
	    	if(offset_y + stroke.h > that.BlockHeight){
	    		areasY = Math.ceil((offset_y + stroke.h)/ that.BlockHeight);
	    	}
	    	
	    	  	
	    	for(var i = 0; i < areasY; i++){
	    		for(var j = 0; j < areasX; j++){;
	    			
	    			var newAreaIndex = areaIndex + that.BlockCols * i + j;
	    			var area2 = that.SolidAreas[newAreaIndex];
	    			
	    			if(typeof area2 === "undefined"){
	    				area2 = new Array();
	    				
	    			}
	    			area2[area2.length] = stroke;
	    			that.SolidAreas[newAreaIndex] = area2;		    			
	    		}
	    	}

	    }
	    //end addSolid



//###<% if(coins){ %>
	    /**
	     * Adicionar uma moeda na cena
	     */
	    Scene.prototype.addCoin = function(col, row, offset_x, offset_y, zoom_x, zoom_y, Cols, Rows, marginR, marginB){

	    	if(typeof that.SolidAreas[that.BlockCols * row + col] === "undefined"){
	    		that.SolidAreas[that.BlockCols * row + col] = new Array();
	    	}
	    	
	    	if(typeof zoom_x === "undefined"){
	    		zoom_x = 1;
	    		zoom_y = 1;    		
	    	}
	    	
	    	if(typeof Cols ===  "undefined"){
	    		Cols = 1;
	    		Rows = 1;    		
	    	}

			if(typeof marginR === "undefined"){
				marginR = 0;
			}

			if(typeof marginB === "undefined"){
				marginB = 0;
			}

//	    	var areaIndex = that.BlockCols * row + col;
	    	//console.log(this.coords);
	    	
	    	var width = this.coords.animation["coin"].w * zoom_x;
			var height = this.coords.animation["coin"].h * zoom_y;
			
			var translate_x = 0;
			var translate_y = 0;
			//var current_height = 0;
	    	
			
			//TODO: entender
			//para cada coluna para cada linha
			//é contada a translacao do objeto no eixo x e no eixo y
			//mas eu não quero fazer dessa forma eu quero colocar varios objetos na cena
	    	for(var i = 0; i < Cols; i++){
	    		translate_y = 0;
	    		for(var j = 0; j < Rows; j++){
	    			
	    			//TODO: é necessário calcular o indice aqui.
	    			//talvez usando a função de indice do player
	    			
	    			/* NOVO:
	    			 * aqui a função para calcular o indice
	    			 * var areaIndex = that.BlockCols * row + col;
	    			 */
	    			var row2 = row + Math.floor((translate_y + offset_y)/this.BlockHeight);
	    			var col2 = col + Math.floor((translate_x + offset_x)/this.BlockWidth);
	    			
	    			
	    			//NOVO:
	    			// funcao para calcular a area ocupada pelo objeto
	    			var areaIndex = that.BlockCols * row2 + col2;
	    			var coinIndex = this.definedCoins.length;
	    			
	    	    	var area = this.SolidAreas[areaIndex];
	    	    	if(typeof area === "undefined"){
	    				area = new Array();
	    			}
	    	    	var itemIndex = area.length;
	    	    	
	    	    	var obj = {
	    	    			"type": "coin",
	    	    			"name" : "coin",
	    	    			"index": 0,
	    	    			"delta": 0,
	    	    			"x": that.BlockWidth * col + offset_x + translate_x,
	    					"y": that.BlockHeight * row + offset_y + translate_y,
	    					"w": width,
	    					"h": height,
	    					"areaIndex": areaIndex,
	    					"coinIndex": coinIndex,  // << usar para deletar do mostrador
	    					"itemIndex": itemIndex
	    	    	};
	    	    	
	    	    	area[itemIndex] = obj;
	    	    	
	    	    	//Adiciona para mostrar
	    	    	this.definedCoins[coinIndex] = obj;
	    	    	

	    	    	
	    	    	//Adiciona na area de colisao
	    	    	// nesta celula há um array de objetos que estão neste indice
	    	    	area[area.length] = obj;
	    	    	
					//na coluna zero
		    		translate_y = translate_y + height + marginB;

	    			//TODO: quando o objeto colidir ele deve remover o stroke e a animacao
		    		
	    		}
	    		
				translate_x = translate_x + width + marginR;
	    		
	    	}
	    	
	    }//end addCoin
//###<% } %>	    

	    //begin add out
	    Scene.prototype.addOut = function(col, row, offset_x, offset_y, zoom_x, zoom_y){

	    	if(typeof that.SolidAreas[that.BlockCols * row + col] === "undefined"){
	    		that.SolidAreas[that.BlockCols * row + col] = new Array();
	    	}
	    	
	    	if(typeof zoom_x === "undefined"){
	    		zoom_x = 1;
	    		zoom_y = 1;    		
	    	}
	    	
	    	var width = this.coords.animation["out"].w * zoom_x;
			var height = this.coords.animation["out"].h * zoom_y;
		
	    			
			var row2 = row + Math.floor(offset_y/this.BlockHeight);
			var col2 = col + Math.floor(offset_x/this.BlockWidth);
			
			// funcao para calcular a area ocupada pelo objeto
			var areaIndex = that.BlockCols * row2 + col2;
			
	    	var area = this.SolidAreas[areaIndex];
	    	if(typeof area === "undefined"){
				area = new Array();
			}
	    	var itemIndex = area.length;
	    	
	    	var obj = {
	    			"type": "out",
	    			"name" : "out",
	    			"index": 0,
	    			"delta": 0,
	    			"x": that.BlockWidth * col + offset_x,
					"y": that.BlockHeight * row + offset_y,
					"w": width,
					"h": height,
					"areaIndex": areaIndex,
					"itemIndex": itemIndex
	    	};
	    	
	    	area[itemIndex] = obj;
	    	
	    	//Adiciona para mostrar
	    	this.outplace = obj;
	    	
	    	//Adiciona na area de colisao
	    	// nesta celula há um array de objetos que estão neste indice
	    	area[area.length] = obj;
	    	
	    }//end add out
	    

	    //TODO: no futuro essa funcao devera ser chamada uma unica vez
	    // e caso necessario os objetos serao redimencionados sem recriar a cena
		/**
	     * @Description funcao para montar a cena
	     * 
	     */
		Scene.prototype.makeScene = function(){
		    
			//TODO: moeda faz parte da cena: variavel publica?
			//TODO: solidos fazem parte da cena: variavel publica?
			
				
			
				var right = true;
				var left = false;
				var jump = true;
				var walk = false;
			
			
				
				
				this.addSolid("question", 0, 4, 0, 30, this.scaleX, this.scaleY, 10, 8);
//###<% if(solid2){ %>
				this.addSolid("question", 2, 4, 45, 17, this.scaleX, this.scaleY, 7, 6);
//###<% } if(solid3){ %>
				this.addSolid("question", 4, 3, 30, 17, this.scaleX, this.scaleY, 4, 1);
//###<% } if(solid4){ %>
				this.addSolid("question", 6, 2, 30, 30, this.scaleX, this.scaleY, 4, 1);
//###<% } if(solid5){ %>
				this.addSolid("question", 5, 5, 30, 50, this.scaleX, this.scaleY, 25, 2);
//###<% } if(coins){ %>
				this.addCoin(4, 3, 10, 50, this.scaleX, this.scaleY, 30, 8, 5, 5);
//###<% } %>	
				
				//this.addSolid("out", 9, 5, this.BlockHeight-30, 50, this.scaleX, this.scaleY, 1, 1);
				this.addOut(9, 5, this.BlockHeight-30, 50, this.scaleX, this.scaleY);
				
//		    	col1, row, limitX, distance, translateX, translateY, toRight
//###<% if(mob1.jump){ %>
				/*> this.addMob(5, 4, 30, 370, 10, 10, right, jump); <*/
//###<%} else{ %>
				this.addMob(5, 4, 30, 370, 10, 10, right, walk);
//###<% } %>
//###<% if(mob2.use){ %>
//###<% if(mob2.jump){ %>
				/*> this.addMob(5, 4, 30, 370, 10, 10, left, jump); <*/
//###<%} else{ %>
		        this.addMob(5, 4, 30, 370, 10, 10, left,  walk);
//###<% } %>
//###<%} if(mob3.use){ %>
//###<% if(mob3.jump){ %>
		        /*> this.addMob(3, 4, 50, 437, 100, 10, right, jump); <*/
//###<%} else{ %>		        
		        this.addMob(3, 4, 50, 437, 100, 10, right, walk);
//###<% } %>			        
//###<%} if(mob4.use){ %>
//###<% if(mob4.jump){ %>
		    	/*> this.addMob(3, 4, 50, 437, 100, 10, left,  jump); <*/
//###<%} else{ %>
		    	this.addMob(3, 4, 50, 437, 100, 10, left,  walk);
//###<% } %>	
		    	
//###<%} if(mob5.use){ %>
//###<% if(mob5.jump){ %>
		    	/*> this.addMob(0, 4, 45, 835, 100, 10, right, jump); <*/
//###<%} else{ %>
		    	this.addMob(0, 4, 45, 835, 100, 10, right, walk);
//###<% } %>
//###<%} if(mob6.use){ %>
//###<% if(mob6.jump){ %>
		    	/*> this.addMob(0, 4, 45, 835, 100, 10, left,  jump); <*/
//###<%} else{ %>
		    	this.addMob(0, 4, 45, 835, 100, 10, left,  walk);
//###<% } %>
//###<% } %>		    	
		    	
		    }
	    
	    
	    
	    Scene.prototype.update = function(delta){
		    	
		    	if(this.readyState){
		    
		    		//console.log(definedSolids);
		    		//////////////////////////////////////
		    		/// SOLIDS
		    		//////////////////////////////////////
		    		for(var i = 0; i < this.definedSolids.length; i++){
		    		var offset = this.definedSolids[i];		


		    		//console.log(this.coords.animation[offset.name]);
		    		var ani = this.coords.animation[offset.name];
		    		
		    		offset.delta += delta;
		    		if(offset.delta > ani.delay){
		    			offset.delta = offset.delta - ani.delay;
			    		if(offset.index < ani.objects.length -1){
			    			offset.index++;
			    		}else{
			    			offset.index = 0;
			    		}
		    		
		    		}
		    		
		    		this.context.drawImage(this.img,
							ani.objects[offset.index].x,
							ani.objects[offset.index].y,
							ani.objects[offset.index].w,
							ani.objects[offset.index].h,
							offset.x + (ani.w - ani.objects[offset.index].w)/2,
							offset.y,
							ani.objects[offset.index].w * this.scaleX,
							ani.objects[offset.index].h * this.scaleY);
		    		
		    		}// end for solids 

//###<% if(coins){ %>
		    		for(var i = 0; i < this.definedCoins.length; i++){
			    		var offset = this.definedCoins[i];		
			    		
			    		if(typeof offset === "undefined"){
			    			continue;
			    		}

			    		//console.log(this.coords.animation[offset.name]);
			    		var ani = this.coords.animation[offset.name];
			    		
			    		offset.delta += delta;
			    		if(offset.delta > ani.delay){
			    			offset.delta = offset.delta - ani.delay;
				    		if(offset.index < ani.objects.length -1){
				    			offset.index++;
				    		}else{
				    			offset.index = 0;
				    		}
			    		
			    		}
			    		
			    		this.context.drawImage(this.img,
								ani.objects[offset.index].x,
								ani.objects[offset.index].y,
								ani.objects[offset.index].w,
								ani.objects[offset.index].h,
								offset.x + (ani.w - ani.objects[offset.index].w)/2,
								offset.y,
								ani.objects[offset.index].w * this.scaleX,
								ani.objects[offset.index].h * this.scaleY);
			    		} // for coins
//###<% } %>
		    	
		    		//saida do jogo
		    		var out = this.coords.animation["out"];
		    		
		    		//console.log(out);
		    		
		    		this.context.drawImage(this.img,
		    				out.objects[0].x,
		    				out.objects[0].y,
		    				out.objects[0].w,
		    				out.objects[0].h,
							this.outplace.x + (out.w - out.objects[0].w)/2,
							this.outplace.y,
							out.objects[0].w * this.scaleX,
							out.objects[0].h * this.scaleY);
		    		
		    	} //end if Ready
		    	else{
		    		//Se a imagem não tiver sido carregada .. mostra o contorno
		    		for(var i = 0; i < this.SolidAreas.length; i++){
			    		var area = this.SolidAreas[i];
		    		
			    		if(typeof area !== "undefined"){
			    		
				    		for(var j = 0; j < area.length; j++){
				    			
				    			this.context.rect(
				    					area[j].x,
				    					area[j].y,
				    					area[j].w,
				    					area[j].h);
								this.context.stroke();
				    			
				    		}
			    		
			    		} // if area
			    		
		    	
		    		} // for
	    	
	    	} //else
	    }
	    
	    Scene.prototype.clear = function(){
//###<% if(coins){ %>
	    	while(this.definedCoins.length > 0){
				var len = this.definedCoins.length - 1;
	    		delete this.definedCoins[len];
	    		this.definedCoins.length = len;
	    	}
//###<% } %>
	    	
	    	while(this.definedSolids.length > 0){
				var len = this.definedSolids.length - 1;
	    		delete this.definedSolids[len];
	    		this.definedSolids.length = len;
	    	}
	    	
		    while(this.mobs.length > 0){
				var len = this.mobs.length - 1;
	    		delete this.mobs[len];
	    		this.mobs.length = len;
	    	}
	    }
	    
	    
	  //saber em que posicao esta um ponto
	    Scene.prototype.getIndex = function(scene, _x, _y){
	    	
	    	var col = Math.floor(_x/scene.BlockWidth);
			var row = Math.floor(_y/scene.BlockHeight);
			
			return scene.BlockCols * row + col;
			
	    }
	    
	    /*
	     * Montar a cena aqui..
	     */
	    Scene.prototype.draw = function(delta){
	    	
	    	//*
	    	Clear(this, '#bbbbff');
	    	Grid(this, '#0000ff');
	    	//*/
	    	
	    	/*
	    	Clear('#bbbbff');
	    	Grid('#000000');
	    	//*/
	    	
	    	
	    	//DrawSolids(this); 
	    	this.update(delta);
	    	    	
	    	if(this.player !== null)
	    		this.player.draw(delta);
	    	
	    	for(var i = 0; i < this.mobs.length; i++){
	    		this.mobs[i].draw(delta);
	    	}
	    	
	    	
	    }   

	    return Scene;

	  })();

	  window.Scene = Scene;

	}).call(this);
