'use strict';

(function () {
	  var Character;
	  
	  Character = (function() {
	    
		//TODO: definir as animações separadamente
		//TODO: uma animação para cada ação do personagem
	    function Character() {
	    	var that = this;
	    	
	    	
	    	this.img = new Image();
	    	
	    	this.scene = null;
	    	this.animation = null;
	    	
	    	/*
	    	 * 30 frames por segundo * segundo = 30
	    	 * 30/1000 * 10000 = 30;
	    	 * ou seja em 1000 eu vou ter 30;
	    	 * dado um tempo N dividir por 1000
	    	 * se esse numero for ou maior a 30/1000
	    	 * reduzir 30/1000 do numero
	    	 * */
	    	
	    	
	    	//this.delta = 0;
	    	this.fps = 1;
	    	
	    	this.delta = 0;
//	    	this.td = 0;
	    	
	    	
	    	
	    	this.animationIndex = 0;
	    	this.readyState = false;
	    	
	    	
	    	//variaveis auxiliares
	    	var ds;
	    	var dt;
	    	
	    	// n pixels por segundo ao quadrado
	    	ds = 300;
	    	dt = 1000; //1s
	    	this.ACCELERATIONY = ds / (dt * dt); //constante
	    	this.accelerationY = this.ACCELERATIONY; //variavel
	    	this.y = 0;
	    	this.velocityY = 0;
	    	
	    	this.VELOCITY_JUMP  = -300 / 1000;
	    	

	    	// n pixels por segundo
	    	ds = 80;
	    	dt = 1000;  //1s
	    	this.VELOCITYX_WALK = ds / dt; //constante
	    	
	    	ds = 110;
	    	dt = 1000;  //1s
	    	this.VELOCITYX_JUMP = ds / dt; //constante
	    	
	    	
	    	this.velocityX = this.VELOCITYX_WALK;
	    	this.x = 100;
	    	
	    	this.CHARACTER_COLISION_NOPE = false;
	    	this.CHARACTER_COLISION_TOP = false;
	    	this.CHARACTER_COLISION_BOTTOM = false;
	    	this.CHARACTER_COLISION_LEFT = false;
	    	this.CHARACTER_COLISION_RIGHT = false;
	    	this.CHARACTER_MOVE_RIGHT = false;
			this.CHARACTER_MOVE_LEFT = false;
			this.CHARACTER_MOVE_UP = false;
			this.CHARACTER_MOVE_DOWN = false;
			this.CHARACTER_OVERFLOW_GROUND = false;
			this.CHARACTER_OVERFLOW_LEFT = false;
			this.CHARACTER_OVERFLOW_RIGHT = false;
			this.CHARACTER_IS_DEAD = false;
			this.CHARACTER_IS_WON = false;
			this.CHARACTER_READY_STATE = false;
			
			this.CURRENT_ANIMATION = "direita";
	    	
	    	//prototype
	    	this.animations = null;
	    	
	    	once(this.img,'load', function(){
	    		that.CHARACTER_READY_STATE = true;
	    	});
	    	
	    }

	    Character.prototype.setContext = function(Context){
	    	context = Context;
	    }
	    
	    Character.prototype.setScene = function(Scene){
	    	tscene = Scene;
	    }
	    
//###<% if(zoom){ %>
	    Character.prototype.fitScreen = function(){

	    	//variaveis auxiliares
	    	var ds;
	    	var dt;
	    	
	    	// n pixels por segundo ao quadrado
	    	ds = 300;
	    	dt = 1000; //1s
	    	this.ACCELERATIONY = ds / (dt * dt); //constante
	    	this.accelerationY = this.ACCELERATIONY; //variavel
	    	this.y = 0;
	    	this.velocityY = 0;
	    	
	    	this.VELOCITY_JUMP  = -300 / 1000 * this.scene.scaleY;
	    	

	    	// n pixels por segundo
	    	ds = 80;
	    	dt = 1000;  //1s
	    	this.VELOCITYX_WALK = ds / dt * this.scene.scaleX; //constante
	    	
	    	ds = 110;
	    	dt = 1000;  //1s
	    	this.VELOCITYX_JUMP = ds / dt * this.scene.scaleY; //constante
	    	
	    	
	    	this.velocityX = this.VELOCITYX_WALK;
	    	this.x = 100;
	    	
	    }
//###<% } %>
 
	    Character.prototype.colideWithPlayer = function (player){
	    	
	    	
	    	if(this.CHARACTER_IS_DEAD || player.CHARACTER_IS_DEAD){
	    		return false;
	    	}
	    	
	    	//adicionar primeiro os pontos da cabeca
	    	var pontos = [
		    	 {
		    		 //cabeca esquerda
		    		 foot : false,
		    		 x: this.x,
		    		 y: this.y
		    	 },
		    	 {
		    		 //cabeca direita
		    		 foot: false,
		    		 x: this.x + (this.w * this.scene.scaleX),
		    		 y: this.y
		    	 },
		    	 {
		    		 //pe direito
		    		 foot: true,
		    		 x: this.x + (this.w * this.scene.scaleX),
		    		 y: this.y + (this.h * this.scene.scaleY)
		    	 },
		    	 {
		    		 //pe esquerdo
		    		 foot: true,
		    		 x: this.x,
		    		 y: this.y + (this.h * this.scene.scaleY)
		    	 }
	    	 ];
	    	
	    	var _cx = this.x + (this.w * this.scene.scaleX / 2);
	    	//var _cy = this.y + (this.h * this.scene.scaleY / 2);
	    	
	    	for(var i = 0; i < 4; i++){
	    		var ponto = pontos[i];
	    		
		    	if ((ponto.x >= player.x) && (ponto.x <= player.x + player.w) && (ponto.y >= player.y) && (ponto.y <= player.y + player.h) ){
		    		
		    		if(ponto.y < toFNgetY(player, 0, _cx)){ //## e\
		    			//left bottom
		    			if(ponto.y > toFNgetY(player, 1, _cx)){ // ## e/
		    				//console.log("L < 0, > 1");
		    				//that.CHARACTER_COLISION_RIGHT = true;
		    				player.CHARACTER_IS_DEAD = true;
		    				player.velocityY = player.VELOCITY_JUMP;
		    				
		    				console.log("1");
		    				return;
		    				
		    			}
		    			else{  // ## /d
		    				//console.log("T < 0, < 1");
		    				
		    				if(ponto.foot){ //identificando que player ponto esta no pe do personagem
		    					//that.CHARACTER_COLISION_BOTTOM = true;
		    					player.CHARACTER_IS_DEAD = true;
		    					player.velocityY = player.VELOCITY_JUMP;
		    					//alert(true);
		    					
		    					console.log("2");
		    					return;
		    					
		    				}
		    				else{
		    					//that.CHARACTER_COLISION_TOP = true;
		    					this.CHARACTER_IS_DEAD = true;
		    					player.velocityY = player.VELOCITY_JUMP;
		    					
		    					console.log(ponto);
		    					
		    					console.log("3");
		    					return;
		    					
		    				}
		    			}
		    		}
		    		else{  //## \d
		    			//top right
		    			if(ponto.y > toFNgetY(player, 1, _cx)){  // ## e/
		    				//console.log("B > 0, > 1");
		    				//that.CHARACTER_COLISION_TOP = true;
		    				this.CHARACTER_IS_DEAD = true;
		    				
		    				console.log("4");
		    				return;
		    				
		    			}
		    			else{  // ## /d
		    				//console.log("R > 0, < 1");
		    				//that.CHARACTER_COLISION_LEFT = true;
		    				
		    				if(ponto.foot){
		    					player.CHARACTER_IS_DEAD = true;
		    					player.velocityY = player.VELOCITY_JUMP;
		    					
		    					console.log("5");
		    					return;
		    					
		    				}
		    				else{
		    					this.CHARACTER_IS_DEAD = true;
		    					player.velocityY = player.VELOCITY_JUMP;
		    					
		    					console.log("6");
		    					return;
		    					
		    				}
		    			}
		    		}
		    		
		    	}  // end if colide
	    	}
	    	return false;
	    }
	    
	    
	    /**
	     * 
	     */
	    Character.prototype.colideWithSolid = function(solid){
	    	
	    	//adicionar primeiro os pontos da cabeca
	    	var pontos = [
		    	 {
		    		 //cabeca esquerda
		    		 foot : false,
		    		 right: false,
		    		 x: this.x,
		    		 y: this.y
		    	 },
		    	 {
		    		 //cabeca direita
		    		 foot: false,
		    		 right: true,
		    		 x: this.x + (this.w * this.scene.scaleX),
		    		 y: this.y
		    	 },
		    	 {
		    		 //pe direito
		    		 foot: true,
		    		 right: true,
		    		 x: this.x + (this.w * this.scene.scaleX),
		    		 y: this.y + (this.h * this.scene.scaleY)
		    	 },
		    	 {
		    		 //pe esquerdo
		    		 foot: true,
		    		 right: false,
		    		 x: this.x,
		    		 y: this.y + (this.h * this.scene.scaleY)
		    	 }
	    	 ];
	    	
	    	var _cx = this.x + (this.w * this.scene.scaleX / 2);
	    	
	    	for(var i = 0; i < 4; i++){
	    		var ponto = pontos[i];
	    		
	    		if(ponto.foot){
	    			this.CHARACTER_COLISION_BOTTOM = false;
	    		}else{
	    			this.CHARACTER_COLISION_TOP = false;
	    		}
	    		
	    		if ((ponto.x >= solid.x) && (ponto.x <= solid.x + solid.w) && (ponto.y >= solid.y) && (ponto.y <= solid.y + solid.h) ){
	    			
	    			
	    			
	    			if(ponto.foot){

						if ((ponto.x >= solid.x) && (ponto.x <= solid.x + solid.w) && (ponto.y >= solid.y) && (ponto.y <= solid.y + 5) ){
		    				this.CHARACTER_COLISION_BOTTOM = true;
		    			}
	    				//else{
	    					//throw "e";
	    				//}
	    			}
	    			
	    			
					this.CHARACTER_COLISION_TOP = true;	    			
	    			
	    			
	    				
    			}

	    	}
	    	
	    }
	    
//###<% if(coins){ %>    
	    //A colisao é simples
	    //ao colidir com a moeda
	    //1. A moeda vai para o personagem
	    //2. A moeda some: é apagada
	    //3. O stroke some
	    Character.prototype.colideWithCoin = function(coin){
	    	
	    	if(this.type === "mob") return;
	    	
	    	var pontos = [
	    			    	 {
	    			    		 //cabeca esquerda
	    			    		 foot : false,
	    			    		 right: false,
	    			    		 x: this.x,
	    			    		 y: this.y
	    			    	 },
	    			    	 {
	    			    		 //cabeca direita
	    			    		 foot: false,
	    			    		 right: true,
	    			    		 x: this.x + (this.w * this.scene.scaleX),
	    			    		 y: this.y
	    			    	 },
	    			    	 {
	    			    		 //pe direito
	    			    		 foot: true,
	    			    		 right: true,
	    			    		 x: this.x + (this.w * this.scene.scaleX),
	    			    		 y: this.y + (this.h * this.scene.scaleY)
	    			    	 },
	    			    	 {
	    			    		 //pe esquerdo
	    			    		 foot: true,
	    			    		 right: false,
	    			    		 x: this.x,
	    			    		 y: this.y + (this.h * this.scene.scaleY)
	    			    	 }
	    		    	 ];
	    	
	    	
	    	for(var i = 0; i < 4; i++){
	    		var ponto = pontos[i];
		    	if ((ponto.x >= coin.x) && (ponto.x <= coin.x + coin.w) && (ponto.y >= coin.y) && (ponto.y <= coin.y + coin.h) ){
		    		
		    		//if(coin.type ==="out"){ alert("Parabens!, você venceu"); return; }
		    		
		    		delete this.scene.definedCoins[coin.coinIndex];
		    		delete (this.scene.SolidAreas[coin.areaIndex])[coin.itemIndex];
		    	}
	    	}
	    }
//###<% } %>
	    
	    
	    
	    Character.prototype.colideWithEnd = function(endPlace){
	    	
	    	if(this.type === "mob") return;
	    	
	    	var pontos = [
	    			    	 {
	    			    		 //cabeca esquerda
	    			    		 foot : false,
	    			    		 right: false,
	    			    		 x: this.x,
	    			    		 y: this.y
	    			    	 },
	    			    	 {
	    			    		 //cabeca direita
	    			    		 foot: false,
	    			    		 right: true,
	    			    		 x: this.x + (this.w * this.scene.scaleX),
	    			    		 y: this.y
	    			    	 },
	    			    	 {
	    			    		 //pe direito
	    			    		 foot: true,
	    			    		 right: true,
	    			    		 x: this.x + (this.w * this.scene.scaleX),
	    			    		 y: this.y + (this.h * this.scene.scaleY)
	    			    	 },
	    			    	 {
	    			    		 //pe esquerdo
	    			    		 foot: true,
	    			    		 right: false,
	    			    		 x: this.x,
	    			    		 y: this.y + (this.h * this.scene.scaleY)
	    			    	 }
	    		    	 ];
	    	
	    	
	    	for(var i = 0; i < 4; i++){
	    		var ponto = pontos[i];
		    	if ((ponto.x >= endPlace.x) && (ponto.x <= endPlace.x + endPlace.w) && (ponto.y >= endPlace.y) && (ponto.y <= endPlace.y + endPlace.h) ){
		    		
		    		if(endPlace.type ==="out" && !this.CHARACTER_IS_WON){ alert("Parabens!, você venceu"); this.CHARACTER_IS_WON = true; return; }
		    		
		    		
		    	}
	    	}
	    }
	    
	    
	    
	    
	    
	    Character.prototype.draw = function(delta) {
	    	
	    	if(this.CHARACTER_IS_DEAD){
	    		if(this.type == "player"){
	    			console.log("character is dead");
	    		}
	    	}
	    		    	
	    	//Usado somente nos personagens automaticos
	    	this.updateMotion(delta);
	    	
	    	var o0 = this.coords.animation[this.CURRENT_ANIMATION];
	    	
			this.w = o0.w;
			this.h = o0.h;
						
			var corners = new Array();
			
			corners[0] = this.scene.getIndex(this.scene, this.x, this.y);
			corners[1] = this.scene.getIndex(this.scene, this.x+this.w, this.y);
			corners[2] = this.scene.getIndex(this.scene, this.x, this.y+this.h);
			corners[3] = this.scene.getIndex(this.scene, this.x+this.w, this.y+this.h);
		
			//console.log(this.scene.player);
			if(this.type == "player"){
				this.scene.PlayerAreas = new Array(this.scene.BlockCols * this.scene.BlockRows);
				this.scene.PlayerAreas[corners[0]] = true;
				this.scene.PlayerAreas[corners[1]] = true;
				this.scene.PlayerAreas[corners[2]] = true;
				this.scene.PlayerAreas[corners[3]] = true;
			}else{
//				if(
//						this.scene.PlayerAreas[corners[0]] ||
//						this.scene.PlayerAreas[corners[1]] ||
//						this.scene.PlayerAreas[corners[2]] ||
//						this.scene.PlayerAreas[corners[3]]
//						)
//				{
				this.colideWithPlayer(this.scene.player);
//				} // if player is arround
			}// if not player
			
			
			for(var i = 0; i < 4; i++){
				var objects = this.scene.SolidAreas[corners[i]];
				if(typeof objects !== "undefined"){
					for(var j = 0; j < objects.length; j++){
						
						if(typeof objects[j] !== "undefined"){

							if(objects[j].type === "solid"){
								this.colideWithSolid(objects[j]);
							}
//###<% if(coins){ %>
							else if(objects[j].type === "coin"){
								this.colideWithCoin(objects[j]);
							}
//###<% } %>
							else{
								this.colideWithEnd(objects[j]);
							}

						}

					}//for objects
				}//if objects defined
			}//for each corner
			
			
//			delete corners[0];
//			delete corners[1];
//			delete corners[2];
//			delete corners[3];
			
////######################################################## movimentação: novo

			
			
	    	//Ação da Gravidade
	    	if(!this.CHARACTER_IS_DEAD && (this.CHARACTER_COLISION_BOTTOM || (this.y + this.h >= 6 * this.scene.BlockHeight))){
	    		//A velocidade de caida e constante igual a zero;
	    		this.velocityY = 0;
	    		//this.velocityX = this.VELOCITX_WALK;
	    		
		    	//Pula somente se estiver colidindo no chão
		    	if(this.CHARACTER_MOVE_UP){
		    		this.velocityX = this.VELOCITYX_JUMP;
		    		this.velocityY = this.VELOCITY_JUMP;
		    		this.y = this.y + this.velocityY * delta + this.accelerationY * delta * delta / 2; //falta ajustar o zoom..
		    		this.velocityY = this.velocityY + this.accelerationY * delta; //velocidade variavel;
		    		//esta parte não pode ser usada no nob
		    		//this.CHARACTER_MOVE_UP = false;
				}
	    		
	    	}
	    	else{
//	    		//A velocidade de caida vai aumentando conforme o tempo;
	    		//this.y = this.y + this.velocityY * delta; //falta ajustar o zoom..
	    		this.y = this.y + this.velocityY * delta + this.accelerationY * delta * delta / 2;
		    	this.velocityY = this.velocityY + this.accelerationY * delta; //velocidade variavel;
	    	}
	    	
	    	//se abaixa
	    	if(this.CHARACTER_COLISION_BOTTOM && this.CHARACTER_MOVE_DOWN){
	    		
			}
	    	
	    	//move para a DIREITA somente se não estiver movendo para a esquerda
	    	if(this.CHARACTER_MOVE_RIGHT){
	    		//console.log("this.CHARACTER_MOVE_RIGHT", this.CHARACTER_MOVE_RIGHT);
	    		this.x += this.velocityX * delta;
	    		//TODO: falta ajustar o zoom..
	    		//TODO: Falta atualizar a animação
	    		//console.log(">>>", this.x);
			}
	    	
	    	//move para a ESQUERDA somente se não estiver movendo para a direita
			if(this.CHARACTER_MOVE_LEFT){
				this.x -= this.velocityX * delta;
				//TODO: falta ajustar o zoom..
				//TODO: Falta atualizar a animação
				//console.log("<<<", this.x);
			}
			
////######################################################## movimentação: novo

			
				//desenhar o proximo frame
				if(this.CHARACTER_READY_STATE){
					
					var o1;
					
					//DEBUG
					if(this.animationIndex >= o0.objects.length){
						this.animationIndex = 0;
					}
					o1 = o0.objects[this.animationIndex];
					
					
					if(this.CHARACTER_MOVE_RIGHT || this.CHARACTER_MOVE_LEFT){
						
						try{
							this.scene.context.drawImage(
								this.img,
								o1.x,
								o1.y,
								o1.w,
								o1.h,
								this.x,
								this.y,
								o1.w * this.scene.scaleX,
								o1.h * this.scene.scaleY
							);
						}
						catch(e){
							throw e;
						}
						
						this.delta += delta;						
						if(this.delta > this.fps){
							
							this.delta = this.delta - this.fps;
													
							if(this.animationIndex < o0.objects.length-1){
								this.animationIndex++;
							}
							else{
								//TODO: so passou uma vez no player								
								this.animationIndex = 0;
							}
							
						}

					}
					else{
						this.scene.context.drawImage(
							this.img,
							o1.x,
							o1.y,
							o1.w,
							o1.h,
							this.x,
							this.y,
							o1.w * this.scene.scaleX,
							o1.h * this.scene.scaleY
						);
					}
				}
				else{
					this.scene.context.rect(
						this.x,
						this.y,
						o0.w * this.scene.scaleX,
						o0.h * this.scene.scaleY
					);
					this.scene.context.stroke();	
				}
	    };
	    

	    return Character;

	  })();

	  window.Character = Character;
	  
}).call(this);
