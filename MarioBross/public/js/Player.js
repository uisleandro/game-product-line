'use strict';

(function() {
	  var Player;
	  
	  Player = (function(_super) {
	    __extends(Player, _super);
	    
		var	KEYS_SPACE = 32,
		KEYS_Q = 81,
		KEYS_W = 87,
		KEYS_E = 69,
		KEYS_A = 65,
		KEYS_S = 83,
		KEYS_D = 68,
		KEYS_LEFT = 37,
		KEYS_UP = 38,
		KEYS_RIGHT = 39,
		KEYS_DOWN = 40,
		KEYS_CONTROL = 17;
	    
	    
	    function Player() {
	    	
	    	Player.__super__.constructor.apply(this, arguments);
	    	var that = this;
	    	
	    	this.coords = {"type":"animation-coords","src":"img/mario4.png","width":1400,"height":36,"animation":
	    	{"direita":
	    	{"loops":0,"length":25,"w":26,"h":35,"objects":[
	    		{"id":0,"delay":1,"x":1,"y":1,"w":23,"h":32,"gx":0,"gy":-1,"gz":0},
	    		{"id":1,"delay":1,"x":29,"y":1,"w":24,"h":31,"gx":0,"gy":-2,"gz":0},
	    		{"id":2,"delay":1,"x":58,"y":1,"w":25,"h":30,"gx":-1,"gy":-2,"gz":0},
	    		{"id":3,"delay":1,"x":88,"y":1,"w":25,"h":30,"gx":-1,"gy":-2,"gz":0},
	    		{"id":4,"delay":1,"x":118,"y":1,"w":26,"h":31,"gx":-1,"gy":-2,"gz":0},
	    		{"id":5,"delay":1,"x":149,"y":1,"w":23,"h":32,"gx":-1,"gy":-1,"gz":0},
	    		{"id":6,"delay":1,"x":177,"y":1,"w":22,"h":32,"gx":0,"gy":0,"gz":0},
	    		{"id":7,"delay":1,"x":204,"y":1,"w":19,"h":32,"gx":-1,"gy":-1,"gz":0},
	    		{"id":8,"delay":1,"x":228,"y":1,"w":17,"h":33,"gx":-3,"gy":-1,"gz":0},
	    		{"id":9,"delay":1,"x":250,"y":1,"w":15,"h":33,"gx":-3,"gy":-1,"gz":0},
	    		{"id":10,"delay":1,"x":270,"y":1,"w":15,"h":33,"gx":-3,"gy":-1,"gz":0},
	    		{"id":11,"delay":1,"x":290,"y":0,"w":16,"h":35,"gx":-2,"gy":0,"gz":0},
	    		{"id":12,"delay":1,"x":311,"y":0,"w":17,"h":35,"gx":-2,"gy":0,"gz":0},
	    		{"id":13,"delay":1,"x":333,"y":1,"w":19,"h":34,"gx":-3,"gy":0,"gz":0},
	    		{"id":14,"delay":1,"x":357,"y":1,"w":23,"h":31,"gx":-4,"gy":-2,"gz":0},
	    		{"id":15,"delay":1,"x":385,"y":1,"w":24,"h":30,"gx":-3,"gy":-2,"gz":0},
	    		{"id":16,"delay":1,"x":414,"y":1,"w":24,"h":30,"gx":-3,"gy":-2,"gz":0},
	    		{"id":17,"delay":1,"x":443,"y":1,"w":24,"h":30,"gx":-3,"gy":-2,"gz":0},
	    		{"id":18,"delay":1,"x":472,"y":1,"w":25,"h":31,"gx":-4,"gy":-2,"gz":0},
	    		{"id":19,"delay":1,"x":502,"y":1,"w":24,"h":33,"gx":-5,"gy":-1,"gz":0},
	    		{"id":20,"delay":1,"x":531,"y":1,"w":24,"h":33,"gx":-5,"gy":-2,"gz":0},
	    		{"id":21,"delay":1,"x":560,"y":1,"w":24,"h":34,"gx":-3,"gy":-1,"gz":0},
	    		{"id":22,"delay":1,"x":589,"y":1,"w":22,"h":34,"gx":-2,"gy":0,"gz":0},
	    		{"id":23,"delay":1,"x":616,"y":1,"w":19,"h":34,"gx":-1,"gy":0,"gz":0},
	    		{"id":24,"delay":1,"x":640,"y":1,"w":23,"h":31,"gx":-1,"gy":-2,"gz":0}]},
	    	"esquerda":
	    	{"loops":0,"length":24,"w":26,"h":35,"objects":[
	    		{"id":0,"delay":1,"x":668,"y":1,"w":23,"h":32,"gx":-1,"gy":0,"gz":0},
	    		{"id":1,"delay":1,"x":725,"y":1,"w":25,"h":30,"gx":0,"gy":-1,"gz":0},
	    		{"id":2,"delay":1,"x":755,"y":1,"w":25,"h":30,"gx":0,"gy":-1,"gz":0},
	    		{"id":3,"delay":1,"x":785,"y":1,"w":26,"h":31,"gx":1,"gy":-1,"gz":0},
	    		{"id":4,"delay":1,"x":816,"y":1,"w":23,"h":32,"gx":0,"gy":0,"gz":0},
	    		{"id":5,"delay":1,"x":844,"y":1,"w":22,"h":32,"gx":0,"gy":1,"gz":0},
	    		{"id":6,"delay":1,"x":871,"y":1,"w":19,"h":32,"gx":0,"gy":0,"gz":0},
	    		{"id":7,"delay":1,"x":895,"y":1,"w":17,"h":33,"gx":1,"gy":0,"gz":0},
	    		{"id":8,"delay":1,"x":917,"y":1,"w":15,"h":33,"gx":2,"gy":0,"gz":0},
	    		{"id":9,"delay":1,"x":937,"y":1,"w":15,"h":33,"gx":2,"gy":0,"gz":0},
	    		{"id":10,"delay":1,"x":957,"y":0,"w":16,"h":35,"gx":2,"gy":1,"gz":0},
	    		{"id":11,"delay":1,"x":978,"y":0,"w":17,"h":35,"gx":1,"gy":1,"gz":0},
	    		{"id":12,"delay":1,"x":1000,"y":1,"w":19,"h":34,"gx":2,"gy":2,"gz":0},
	    		{"id":13,"delay":1,"x":1024,"y":1,"w":23,"h":31,"gx":3,"gy":0,"gz":0},
	    		{"id":14,"delay":1,"x":1052,"y":1,"w":24,"h":30,"gx":3,"gy":0,"gz":0},
	    		{"id":15,"delay":1,"x":1081,"y":1,"w":24,"h":30,"gx":3,"gy":0,"gz":0},
	    		{"id":16,"delay":1,"x":1110,"y":1,"w":24,"h":30,"gx":3,"gy":-1,"gz":0},
	    		{"id":17,"delay":1,"x":1139,"y":1,"w":25,"h":31,"gx":3,"gy":0,"gz":0},
	    		{"id":18,"delay":1,"x":1169,"y":1,"w":24,"h":33,"gx":4,"gy":0,"gz":0},
	    		{"id":19,"delay":1,"x":1198,"y":1,"w":24,"h":33,"gx":5,"gy":0,"gz":0},
	    		{"id":20,"delay":1,"x":1227,"y":1,"w":24,"h":34,"gx":3,"gy":1,"gz":0},
	    		{"id":21,"delay":1,"x":1256,"y":1,"w":22,"h":34,"gx":2,"gy":2,"gz":0},
	    		{"id":22,"delay":1,"x":1283,"y":1,"w":19,"h":34,"gx":0,"gy":2,"gz":0},
	    		{"id":23,"delay":1,"x":1307,"y":1,"w":23,"h":31,"gx":0,"gy":0,"gz":0}]}}}
	    	
	    	this.img.src = this.coords.src;
	    	
	    	this.fps = 30;
	    	
	    	this.y = 200;
	    	this.x = 10;
	    		    	
	    	this.keys = [];
	    	once(document, "keydown", function (e) {
	    		switch (e.keyCode) {
	    			case KEYS_SPACE:
	    				that.CHARACTER_MOVE_UP = true;
	    				break;
	    			case KEYS_Q:
	    			case KEYS_W:
	    			case KEYS_E:
	    			case KEYS_A:
	    			case KEYS_S:
	    			case KEYS_D:
	    			case KEYS_LEFT:
	    				that.turnLeft();
	    				break;
	    			case KEYS_UP:
	    			case KEYS_RIGHT:
	    				that.turnRight();
	    				break;
	    			case KEYS_DOWN:
	    			case KEYS_CONTROL:
	    			//thisPlayer.keys[e.keyCode] = e.keyCode;
	    		}
	    		return false;
	    	});

	    	once(document, "keyup", function (e) {
	    		
	    		switch (e.keyCode) {
	    		case KEYS_SPACE:
	    			that.CHARACTER_MOVE_UP = false;
					break;
				case KEYS_Q:
				case KEYS_W:
				case KEYS_E:
				case KEYS_A:
				case KEYS_S:
				case KEYS_D:
				case KEYS_LEFT:
					that.CHARACTER_MOVE_LEFT = false;
					break;
				case KEYS_UP:
				case KEYS_RIGHT:
					that.CHARACTER_MOVE_RIGHT = false;
					break;
				case KEYS_DOWN:
				case KEYS_CONTROL:
	    			//thisPlayer.keys[e.keyCode] = false;
	    		}
	    		return false;
	    	});

	    	
	      return ;
	    }

	    Player.prototype.type = "player";
	    
	    Player.prototype.turnLeft = function(){
	    	if(!this.CHARACTER_MOVE_LEFT){
		    	this.animationIndex = 0;
		    	this.CHARACTER_MOVE_LEFT = true;
		    	this.CURRENT_ANIMATION = "esquerda";
	    	}
	    }
	    
	    Player.prototype.turnRight = function(){
	    	if(!this.CHARACTER_MOVE_RIGHT){
		    	this.animationIndex = 0;
		    	this.CHARACTER_MOVE_RIGHT = true;
		    	this.CURRENT_ANIMATION = "direita";				
			}
	    }
	    
	    Player.prototype.jump = function(){
	    	this.CHARACTER_MOVE_UP = true;
	    }
	    
	    
//    	se essa funcao nao existir vai gerar um erro
	    Player.prototype.updateMotion = function(delta){
    	}
	    
	    //não é necessario colocar o delta
	    
	    return Player;

	  })(Character);
	  
	  window.Player = Player;
	  
}).call(this);