"use strict";

var fs = require('fs'),
    xml2js = require('xml2js'),
    ejs = require('ejs'),
    async = require('async');

var parser = new xml2js.Parser();


function mandatory(prev, curr, obj){
	
	if(curr){
		for(var i = 0; i < curr.length; i++){
			if(curr[i].mandatory){
				mandatory(curr[i], curr[i].mandatory, obj);
			}
			if(curr[i].optional){
				optional(curr[i], curr[i].optional, obj);
			}
		}
	}
	
}

function optional(prev, curr, obj){
	
	if(typeof curr !== "undefined"){
		
		for(var i = 0; i < curr.length; i++){
			
			if(curr[i].$.name)
				obj[curr[i].$.name] = (curr[i].$.use === "true");
			else{
				
				if(curr[i].$.parent)
					var p = {};
					p[curr[i].$.type] = (curr[i].$.use === "true");
					
					if(typeof prev.$.use !== "undefined")
						p["use"] = (prev.$.use === "true");
					//obj[curr[i].$.parent] = p;
					obj[prev.$.name] = p;
				//console.log("prev", prev.$.name);
			}
			
			if(curr[i].mandatory){
				mandatory(curr[i], curr[i].mandatory, obj);
			}
			if(curr[i].optional){
				optional(curr[i], curr[i].optional, obj);
			}
		}
	}
	
}


fs.readFile(__dirname + '/gen.xml',function(err, data) {
	
	var sdata = data.toString();
	
	parser.parseString(sdata,function(err, result) {
	
		if(err) throw err;
		
		var curr = result.mandatory;
		
		var result = {}
		
		optional(curr, curr.optional, result);
		mandatory(curr, curr.mandatory, result);
		
		var jsfolder = "public/js";
		
		var funcoes = [];
		
//		funcoes[funcoes.length] = function (callback){
//			fs.readFile(jsfolder + '/Util.js',function(err, data2) {
//				var sdata2 = data2.toString();
//				callback(null, sdata2);
//			});
//		}
		
		console.log(result);
		
		funcoes[funcoes.length] = function (callback){
			fs.readFile(jsfolder + '/Game.js',function(err, data2) {
				var sdata2 = data2.toString();
				var contents = ejs.render(sdata2, result)
				.replace(/\'use strict\';/g,'').replace(/\/\*\>/g,'').replace(/\<\*\//g,'');
				callback(null, contents);
			});
		}
		
		funcoes[funcoes.length] = function (callback){
			fs.readFile(jsfolder + '/Scene.js',function(err, data2) {
				var sdata2 = data2.toString();
				var contents = ejs.render(sdata2, result)
				.replace(/\'use strict\';/g,'').replace(/\/\*\>/g,'').replace(/\<\*\//g,'');
				callback(null, contents);
			});
		}
		
		funcoes[funcoes.length] = function (callback){
			fs.readFile(jsfolder + '/Character.js',function(err, data2) {
				var sdata2 = data2.toString();
				var contents = ejs.render(sdata2, result).replace(/\n\/\/###/g,'')
				.replace(/\'use strict\';/g,'').replace(/\/\*\>/g,'').replace(/\<\*\//g,'');
				callback(null, contents);
			});
		}
		
		funcoes[funcoes.length] = function (callback){
			fs.readFile(jsfolder + '/Player.js',function(err, data2) {
				var sdata2 = data2.toString();
				var contents = ejs.render(sdata2, result)
				.replace(/\'use strict\';/g,'').replace(/\/\*\>/g,'').replace(/\<\*\//g,'');
				callback(null, contents);
			});
		}
		
		funcoes[funcoes.length] = function (callback){
			fs.readFile(jsfolder + '/Mob.js',function(err, data2) {
				var sdata2 = data2.toString();
				var contents = ejs.render(sdata2, result)
				.replace(/\'use strict\';/g,'').replace(/\/\*\>/g,'').replace(/\<\*\//g,'');
				callback(null, contents);
			});
		}
		
		
		async.series(funcoes,function(err, results){
		
			var contents = "";
			for(var i = 0; i < results.length; i++){
				contents += results[i];
			}
			
			fs.writeFile(jsfolder + "/generated.js", contents, function(err) {
				if(err) {
					console.log(err);
				} else {
					console.log("SUCCESS");
				}
			});
			
			
			
		});
		
		
	});
	
	
	
	
});
