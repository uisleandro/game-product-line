

//begin game
(function() {
  var Game;
  Game = (function() {
    var that;
    
    var scene = null;
    var canvas = null;
    var context = null;
    
    function Game(CanvasId) {
    	var that = this;
    	
    	this.player = null;
    	
    	this.controls = [];
    	
    	this.mobile = mobilecheck();
    	
    	//Será atualizado
		canvas = document.getElementById(CanvasId);
		
		//Sera atualizado
		context = canvas.getContext("2d");
		
		if(typeof once === "function"){

//###
			once(window,'resize', function(){
				//coloca a cena e o canvas do tamanho da janela
				that.fitScreen();
			});
//###
			
//###
	
			var touches = new Array();
			
			//
			
			
	
			var startMoving = function(e){
				for(var j = 0; j < e.touches.length; j++){
					for(var i = 0; i < that.controls.length; i++){
						if(isPointIncircle(e.touches[j].pageX, e.touches[j].pageY, that.controls[i].x, that.controls[i].y, that.controls[i].r)){
							if(that.player){
								if(that.controls[i].action === "LEFT"){
									touches[i] = "LEFT";
									that.player.CHARACTER_MOVE_RIGHT = false;
									that.player.turnLeft();
								}
								else if(that.controls[i].action === "RIGHT"){
									touches[i] = "RIGHT";
									that.player.CHARACTER_MOVE_LEFT = false;
									that.player.turnRight();
								}
								else if(that.controls[i].action === "JUMP_LEFT"){
									touches[i] = "JUMP_LEFT";
									that.player.jump();
								}
								else if(that.controls[i].action === "JUMP_RIGHT"){
									touches[i] = "JUMP_RIGHT";
									that.player.jump();
								}
							} //if
						} // if
					}//for i
				}//for j
				
			};
			
//			//move
//			var processMove = function(e){
//				
//				//se foi pressionado
//				if(that.pressing){
//					that.pressing = false;
//					for(var i = 0; i < that.controls.length; i++){
//						//se ainda esta pressionado
//						if(isPointIncircle(e.x, e.y, that.controls[i].x, that.controls[i].y, that.controls[i].r)){
//							that.pressing = true;
//						}
//					}
//					
//					if(!that.pressing){						
//						that.player.CHARACTER_MOVE_LEFT = false;
//						that.player.CHARACTER_MOVE_RIGHT = false;
//						that.player.CHARACTER_MOVE_UP = false;
//					}
//					
//				}
//				
//			};
			
			
			var endMoving = function(e){
				
				for(var j = 0; j < e.touches.length; j++){
					for(var i = 0; i < that.controls.length; i++){
						if(isPointIncircle(e.touches[j].pageX, e.touches[j].pageY, that.controls[i].x, that.controls[i].y, that.controls[i].r)){
							if(touches[i] === "LEFT"){
								touches[i] = "";
								that.player.CHARACTER_MOVE_LEFT = false;
							}
							else if(touches[i] === "RIGHT"){
								touches[i] = "";
								that.player.CHARACTER_MOVE_RIGHT = false;	
							}
							else if(touches[i] === "JUMP_LEFT"){
								touches[i] = "";
								//that.player.CHARACTER_MOVE_LEFT = false;
								that.player.CHARACTER_MOVE_UP = false;
							}
							else if(touches[i] === "JUMP_RIGHT"){
								touches[i] = "";
								//that.player.CHARACTER_MOVE_RIGHT = false;
								that.player.CHARACTER_MOVE_UP = false;
							}
						}
					}
				}
				
			};
			
			once(canvas, "touchstart", startMoving);
			once(canvas, "touchend", endMoving);
			
//			once(canvas, "touchmove", processMove);

//###

//###


		}
		else{
			throw "once is not defined";
			exit(1);
		}
		
		this.now = Date.now();
	    this.then = this.now;
	    
	    this.GAME_RUNNING = false;
	    
	    
		this.setScene(new Scene(this));
		
		this.loop();
	    
		
    }

//###
    /**
     * @Description coloca a cena e o canvas do tamanho da janela
     */
    Game.prototype.fitScreen = function(){
    	canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
		
		if(scene != null){
			
			//scene.resize(canvas.width, canvas.height);
			scene.fitScreen();
			scene.clear();
			scene.makeScene();
		}
    	else
    		throw "in 'Game' scene is null\nplease use: game.setScene(Scene)";
		
		
		return that;
    }
//###
    
    Game.prototype.setScene = function(Scene){
    	
    	//var that = this;
    	
    	scene = Scene;
    	scene.canvas = canvas;
    	scene.context = context;
    	
//###
    	this.fitScreen();
//###

//###
    	this.controls = [
			{ //TR
				x:scene.Width-60,
				y: scene.Height -200,
				r: 50,
				action: "JUMP_RIGHT"
			},
			{ //BR
				x:scene.Width-90,
				y:scene.Height -80,
				r:70,
				action: "RIGHT"
			},    	                 
			{ //TL
				x:60,
				y:scene.Height -200,
				r:50,
				action: "JUMP_LEFT"
			},
			{ //BL
				x:90,
				y:scene.Height -80,
				r:70,
				action: "LEFT"
			}
    	];
//###
    	
    	return that;
    }    
    
//###
    Game.prototype.drawControls = function(){
    	
    	for(var i = 0; i < this.controls.length; i++){
    		Circle(context, this.controls[i].x, this.controls[i].y, this.controls[i].r);
    	}
    }
//###
    
    
    Game.prototype.loop = function() {
    	
    	if(!this.GAME_RUNNING){
    		this.GAME_RUNNING = true;
    		
    		this.now = Date.now();
	    	this.then = this.now;
	    	
    		window.requestAnimationFrame(this.loop.bind(this));
    		
    		return;
    	}
    	
    	this.now = Date.now();
    	var delta = (this.now - this.then);
    	this.then = this.now;
    	
    	if(scene != null){
    		//console.log(delta);
    		scene.draw(delta);
//###
    		this.drawControls();
//###
    	}
    	else{
    		throw "in 'Game' scene is null\nplease use: game.setScene(Scene)";
    	}
    	
    	window.requestAnimationFrame(this.loop.bind(this));
    };

    return Game;

  })();

  window.Game = Game;

}).call(this);

(function() {
	  var Scene;

	  Scene = (function() {
	    var that;
	    
	    function Scene(game) {
	    	that = this;
	    	
	    	this.player = null;
	    	
	    	this.game = game;
	    	
	    	this.definedSolids = new Array();
//###
	    	this.definedCoins = new Array();
//###
	    	
	    	this.outplace = null;
	    	
	    	this.mobs = new Array();
	    		    	
	    	this.coords = {"type":"animation-coords","src":"img/solids1.png","width":662,"height":40,"animation":
	    	{"coin":
	    	{"loops":0,"length":4,"w":12,"h":16,
	    		"delay": 150,
	    		"objects":[
	    		{"id":0,"delay":5,"x":158,"y":10,"w":12,"h":16,"gx":0,"gy":0,"gz":0},
	    		{"id":1,"delay":5,"x":180,"y":10,"w":8,"h":16,"gx":0,"gy":0,"gz":0},
	    		{"id":2,"delay":5,"x":198,"y":10,"w":6,"h":16,"gx":0,"gy":0,"gz":0},
	    		{"id":3,"delay":5,"x":214,"y":10,"w":8,"h":16,"gx":0,"gy":0,"gz":0}]},
	    	"question":
	    	{"loops":0,"length":3,"w":16,"h":16,
	    		"delay": 160,
	    		"objects":[
	    		{"id":0,"delay":5,"x":258,"y":10,"w":16,"h":16,"gx":0,"gy":0,"gz":0},
	    		{"id":1,"delay":5,"x":284,"y":10,"w":16,"h":16,"gx":0,"gy":0,"gz":0},
	    		{"id":2,"delay":5,"x":310,"y":10,"w":16,"h":16,"gx":0,"gy":0,"gz":0}]},
	    	"music":
	    	{"loops":0,"length":3,"w":16,"h":16,
	    		"delay": 30,
	    		"objects":[
	    		{"id":0,"delay":5,"x":336,"y":10,"w":16,"h":16,"gx":0,"gy":0,"gz":0},
	    		{"id":1,"delay":5,"x":362,"y":10,"w":16,"h":16,"gx":0,"gy":0,"gz":0},
	    		{"id":2,"delay":5,"x":388,"y":10,"w":16,"h":16,"gx":0,"gy":0,"gz":0}]},
	    	"floor1":
	    	{"loops":0,"length":1,"w":16,"h":16,
	    		"delay": 30,
	    		"objects":[
	    		{"id":0,"delay":5,"x":232,"y":10,"w":16,"h":16,"gx":0,"gy":0,"gz":0}]},
	    	"door":
	    	{"loops":0,"length":1,"w":16,"h":33,
	    		"delay": 30,
	    		"objects":[
	    		{"id":0,"delay":1,"x":624,"y":0,"w":16,"h":33,"gx":0,"gy":-3,"gz":0}]},
	    	"out":
	    	{"loops":0,"length":1,"w":32,"h":24,
	    		"delay": 30,
	    		"objects":[
	    		{"id":0,"delay":1,"x":492,"y":10,"w":32,"h":24,"gx":1,"gy":-2,"gz":0}]},
	    	"coin2":
	    	{"loops":0,"length":2,"w":16,"h":25,
	    		"delay": 30,
	    		"objects":[
	    		{"id":0,"delay":10,"x":466,"y":10,"w":16,"h":25,"gx":0,"gy":-1,"gz":0},
	    		{"id":1,"delay":10,"x":440,"y":10,"w":16,"h":25,"gx":0,"gy":-1,"gz":0}]},
	    	"exclamation":
	    	{"loops":0,"length":1,"w":16,"h":16,
	    		"delay": 30,
	    		"objects":[
	    		{"id":0,"delay":1,"x":414,"y":10,"w":16,"h":16,"gx":0,"gy":0,"gz":0}]},
	    	"floor2":
	    	{"loops":0,"length":4,"w":16,"h":16,
	    		"delay": 30,
	    		"objects":[
	    		{"id":0,"delay":5,"x":534,"y":10,"w":16,"h":16,"gx":0,"gy":0,"gz":0},
	    		{"id":1,"delay":5,"x":560,"y":30,"w":16,"h":10,"gx":0,"gy":0,"gz":0},
	    		{"id":2,"delay":5,"x":10,"y":31,"w":16,"h":4,"gx":0,"gy":0,"gz":0},
	    		{"id":3,"delay":5,"x":560,"y":10,"w":16,"h":10,"gx":0,"gy":0,"gz":0}]}}}

	    	this.context = null;
	    	//this.canvas = null;
	    	
	    	this.readyState = false;
	    	this.img = new Image();
	    	
	    	once(that.img,'load', function(){
	    		that.readyState = true;
	    	});
	    	
	    	
	    	that.img.src = this.coords.src;
	    	
	    	this.BlockCols = 10;
	    	this.BlockRows = 6;
	    	
	    	
	    	this.SolidAreas = new Array(this.BlockCols * this.BlockRows);
	    	this.PlayerAreas = new Array(this.BlockCols * this.BlockRows);

/*	    	
	    	this.Width = 16*60;
	    	this.Height = 9*60;
*/
				this.Width = 480;
	    	this.Height = 854;

				//this.Width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
				//this.Height = (window.innerHeight > 0) ? window.innerHeight : screen.height;
	    	
	    	this.scaleX = 1;
	    	this.scaleY = 1;
	    	
	    	this.BlockWidth = this.Width / this.BlockCols;
	    	this.BlockHeight = this.Height / this.BlockRows;
	    	
	    	this.addPlayer();
	    }    
	    
	    Scene.prototype.addPlayer = function(){
	    	//para que seja possivel colocar os controles
	    	var player = new Player();
	    	this.game.player = player;
	    	player.scene = this;
	    	this.player = player;
	    }
	    
	    /*
	     * o mob atualiza sua posição entrando numa àrea
	     * o player verifica se está colidindo dentro da sua própria área
	     * */
	    
	    /**
	     * @Param Character character
	     * */
	    Scene.prototype.addMob = function(col1, row, limitX, distance, translateX, translateY, toRight, jump){
	    	
			var mob = new Mob();
	    	
	    	mob.scene = this;
	    	mob.delimite(col1, row, limitX, distance, translateX, translateY, toRight);
	    	
	    	if(jump){
	    		mob.CHARACTER_MOVE_UP = true;
	    	}
	    	
	    	
	    	this.mobs[this.mobs.length] = mob;
	    	
	    	
	    }
	    
//###
	    Scene.prototype.fitScreen = function(){
	    	
	    	this.SolidAreas.length = 0;
	    	this.definedSolids.length = 0;
	    	
	    	this.Width = 16*60;
	    	this.Height = 9*60;
	    	
	    	this.scaleX = window.innerWidth / this.Width;
	    	this.scaleY = window.innerHeight / this.Height;
	    	
	    	this.BlockWidth = this.Width / this.BlockCols * this.scaleX;
	    	this.BlockHeight = this.Height / this.BlockRows * this.scaleY;
	    	
	    	this.Width = window.innerWidth;
	    	this.Height = window.innerHeight;
	    	
	    	if(this.player)
	    		this.player.fitScreen();
	    	
	    	for(var i = 0; i < this.mobs.length; i++){
	    		this.mobs[i].fitScreen();
	    	}

	    }
//###
	    	    
		 /**
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
		 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
	     * @Description coloca um indice de um objeto solido em uma area, posiciona o objeto relativo a esta area
	     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
	     * */
	    Scene.prototype.addSolid = function(name, col, row, offset_x, offset_y, zoom_x, zoom_y, Cols, Rows){

	    	if(typeof that.SolidAreas[that.BlockCols * row + col] === "undefined"){
	    		that.SolidAreas[that.BlockCols * row + col] = new Array();
	    	}
	    	
	    	if(typeof zoom_x === "undefined"){
	    		zoom_x = 1;
	    		zoom_y = 1;    		
	    	}
	    	
	    	if(typeof Cols ===  "undefined"){
	    		Cols = 1;
	    		Rows = 1;    		
	    	}
	    	
	    	
	    	var areaIndex = that.BlockCols * row + col;

	    	//console.log(this.coords);
	    	
	    	var width = this.coords.animation[name].w * zoom_x;
			var height = this.coords.animation[name].h * zoom_y;
			
			var translate_x = 0;
			var translate_y = 0;
			//var current_height = 0;
	    	
	    	for(var i = 0; i < Cols; i++){
	    		
	    		translate_y = 0;
	    		for(var j = 0; j < Rows; j++){
	    			//TODO: criar um objeto
	    	    	//varios objetos podem ocupar a mesma area??
	    	    	var obj = {
	    	    			"type": "solid",
	    	    			"name" : name,
	    	    			"index": 0,
	    	    			"delta": 0,
	    	    			"x": that.BlockWidth * col + offset_x + translate_x,
	    					"y": that.BlockHeight * row + offset_y + translate_y,
	    					"w": this.coords.animation[name].w * zoom_x,
	    					"h": this.coords.animation[name].h * zoom_y,
	    					"col": col,
	    					"row": row
	    	    	};
	    	    	

	    	    	
	    	    	//Adiciona para mostrar
	    	    	this.definedSolids[this.definedSolids.length] = obj;
	    	    	
	    	    	//na coluna zero
	    			translate_y += height;
	    			
	    		}
	    		translate_x += width -1;
	    	}
	    	
	    	var stroke = {
	 	    			"type": "solid", //TODO: rever se vai ficar desse jeito
	 	    			"name" : name,
	 	    			"x": that.BlockWidth * col + offset_x,
	 					"y": that.BlockHeight * row + offset_y,
	 					"x2": that.BlockWidth * col + offset_x + translate_x,
	 					"y2": that.BlockHeight * row + offset_y + translate_y,
	 					"cx": that.BlockWidth * col + offset_x + translate_x / 2,
	 					"cy": that.BlockHeight * row + offset_y + translate_y / 2,
	 					"w": translate_x,
	 					"h": translate_y    			
	    	};
	    	
	    		    	
	    	var areasX = 1, areasY = 1;
	    	
	    	if(offset_x + stroke.w > that.BlockWidth){	    		
	    		areasX = Math.ceil( (offset_x + stroke.w) / that.BlockWidth);
	    	}
	    	
	    	if(offset_y + stroke.h > that.BlockHeight){
	    		areasY = Math.ceil((offset_y + stroke.h)/ that.BlockHeight);
	    	}
	    	
	    	  	
	    	for(var i = 0; i < areasY; i++){
	    		for(var j = 0; j < areasX; j++){;
	    			
	    			var newAreaIndex = areaIndex + that.BlockCols * i + j;
	    			var area2 = that.SolidAreas[newAreaIndex];
	    			
	    			if(typeof area2 === "undefined"){
	    				area2 = new Array();
	    				
	    			}
	    			area2[area2.length] = stroke;
	    			that.SolidAreas[newAreaIndex] = area2;		    			
	    		}
	    	}

	    }
	    //end addSolid



//###
	    /**
	     * Adicionar uma moeda na cena
	     */
	    Scene.prototype.addCoin = function(col, row, offset_x, offset_y, zoom_x, zoom_y, Cols, Rows, marginR, marginB){

	    	if(typeof that.SolidAreas[that.BlockCols * row + col] === "undefined"){
	    		that.SolidAreas[that.BlockCols * row + col] = new Array();
	    	}
	    	
	    	if(typeof zoom_x === "undefined"){
	    		zoom_x = 1;
	    		zoom_y = 1;    		
	    	}
	    	
	    	if(typeof Cols ===  "undefined"){
	    		Cols = 1;
	    		Rows = 1;    		
	    	}

			if(typeof marginR === "undefined"){
				marginR = 0;
			}

			if(typeof marginB === "undefined"){
				marginB = 0;
			}

//	    	var areaIndex = that.BlockCols * row + col;
	    	//console.log(this.coords);
	    	
	    	var width = this.coords.animation["coin"].w * zoom_x;
			var height = this.coords.animation["coin"].h * zoom_y;
			
			var translate_x = 0;
			var translate_y = 0;
			//var current_height = 0;
	    	
			
			//TODO: entender
			//para cada coluna para cada linha
			//é contada a translacao do objeto no eixo x e no eixo y
			//mas eu não quero fazer dessa forma eu quero colocar varios objetos na cena
	    	for(var i = 0; i < Cols; i++){
	    		translate_y = 0;
	    		for(var j = 0; j < Rows; j++){
	    			
	    			//TODO: é necessário calcular o indice aqui.
	    			//talvez usando a função de indice do player
	    			
	    			/* NOVO:
	    			 * aqui a função para calcular o indice
	    			 * var areaIndex = that.BlockCols * row + col;
	    			 */
	    			var row2 = row + Math.floor((translate_y + offset_y)/this.BlockHeight);
	    			var col2 = col + Math.floor((translate_x + offset_x)/this.BlockWidth);
	    			
	    			
	    			//NOVO:
	    			// funcao para calcular a area ocupada pelo objeto
	    			var areaIndex = that.BlockCols * row2 + col2;
	    			var coinIndex = this.definedCoins.length;
	    			
	    	    	var area = this.SolidAreas[areaIndex];
	    	    	if(typeof area === "undefined"){
	    				area = new Array();
	    			}
	    	    	var itemIndex = area.length;
	    	    	
	    	    	var obj = {
	    	    			"type": "coin",
	    	    			"name" : "coin",
	    	    			"index": 0,
	    	    			"delta": 0,
	    	    			"x": that.BlockWidth * col + offset_x + translate_x,
	    					"y": that.BlockHeight * row + offset_y + translate_y,
	    					"w": width,
	    					"h": height,
	    					"areaIndex": areaIndex,
	    					"coinIndex": coinIndex,  // << usar para deletar do mostrador
	    					"itemIndex": itemIndex
	    	    	};
	    	    	
	    	    	area[itemIndex] = obj;
	    	    	
	    	    	//Adiciona para mostrar
	    	    	this.definedCoins[coinIndex] = obj;
	    	    	

	    	    	
	    	    	//Adiciona na area de colisao
	    	    	// nesta celula há um array de objetos que estão neste indice
	    	    	area[area.length] = obj;
	    	    	
					//na coluna zero
		    		translate_y = translate_y + height + marginB;

	    			//TODO: quando o objeto colidir ele deve remover o stroke e a animacao
		    		
	    		}
	    		
				translate_x = translate_x + width + marginR;
	    		
	    	}
	    	
	    }//end addCoin
//###	    

	    //begin add out
	    Scene.prototype.addOut = function(col, row, offset_x, offset_y, zoom_x, zoom_y){

	    	if(typeof that.SolidAreas[that.BlockCols * row + col] === "undefined"){
	    		that.SolidAreas[that.BlockCols * row + col] = new Array();
	    	}
	    	
	    	if(typeof zoom_x === "undefined"){
	    		zoom_x = 1;
	    		zoom_y = 1;    		
	    	}
	    	
	    	var width = this.coords.animation["out"].w * zoom_x;
			var height = this.coords.animation["out"].h * zoom_y;
		
	    			
			var row2 = row + Math.floor(offset_y/this.BlockHeight);
			var col2 = col + Math.floor(offset_x/this.BlockWidth);
			
			// funcao para calcular a area ocupada pelo objeto
			var areaIndex = that.BlockCols * row2 + col2;
			
	    	var area = this.SolidAreas[areaIndex];
	    	if(typeof area === "undefined"){
				area = new Array();
			}
	    	var itemIndex = area.length;
	    	
	    	var obj = {
	    			"type": "out",
	    			"name" : "out",
	    			"index": 0,
	    			"delta": 0,
	    			"x": that.BlockWidth * col + offset_x,
					"y": that.BlockHeight * row + offset_y,
					"w": width,
					"h": height,
					"areaIndex": areaIndex,
					"itemIndex": itemIndex
	    	};
	    	
	    	area[itemIndex] = obj;
	    	
	    	//Adiciona para mostrar
	    	this.outplace = obj;
	    	
	    	//Adiciona na area de colisao
	    	// nesta celula há um array de objetos que estão neste indice
	    	area[area.length] = obj;
	    	
	    }//end add out
	    

	    //TODO: no futuro essa funcao devera ser chamada uma unica vez
	    // e caso necessario os objetos serao redimencionados sem recriar a cena
		/**
	     * @Description funcao para montar a cena
	     * 
	     */
		Scene.prototype.makeScene = function(){
		    
			//TODO: moeda faz parte da cena: variavel publica?
			//TODO: solidos fazem parte da cena: variavel publica?
			
				
			
				var right = true;
				var left = false;
				var jump = true;
				var walk = false;
			
			
				
				
				this.addSolid("question", 0, 4, 0, 30, this.scaleX, this.scaleY, 10, 8);
//###
				this.addSolid("question", 2, 4, 45, 17, this.scaleX, this.scaleY, 7, 6);
//###
				this.addSolid("question", 4, 3, 30, 17, this.scaleX, this.scaleY, 4, 1);
//###
				this.addSolid("question", 6, 2, 30, 30, this.scaleX, this.scaleY, 4, 1);
//###
				this.addSolid("question", 5, 5, 30, 50, this.scaleX, this.scaleY, 25, 2);
//###
				this.addCoin(4, 3, 10, 50, this.scaleX, this.scaleY, 30, 8, 5, 5);
//###	
				
				//this.addSolid("out", 9, 5, this.BlockHeight-30, 50, this.scaleX, this.scaleY, 1, 1);
				this.addOut(9, 5, this.BlockHeight-30, 50, this.scaleX, this.scaleY);
				
//		    	col1, row, limitX, distance, translateX, translateY, toRight
//###
				 this.addMob(5, 4, 30, 370, 10, 10, right, jump); 
//###
//###
//###
				 this.addMob(5, 4, 30, 370, 10, 10, left, jump); 
//###
//###
//###
		         this.addMob(3, 4, 50, 437, 100, 10, right, jump); 
//###			        
//###
//###
		    	 this.addMob(3, 4, 50, 437, 100, 10, left,  jump); 
//###	
		    	
//###
//###
		    	 this.addMob(0, 4, 45, 835, 100, 10, right, jump); 
//###
//###
//###
		    	 this.addMob(0, 4, 45, 835, 100, 10, left,  jump); 
//###
//###		    	
		    	
		    }
	    
	    
	    
	    Scene.prototype.update = function(delta){
		    	
		    	if(this.readyState){
		    
		    		//console.log(definedSolids);
		    		//////////////////////////////////////
		    		/// SOLIDS
		    		//////////////////////////////////////
		    		for(var i = 0; i < this.definedSolids.length; i++){
		    		var offset = this.definedSolids[i];		


		    		//console.log(this.coords.animation[offset.name]);
		    		var ani = this.coords.animation[offset.name];
		    		
		    		offset.delta += delta;
		    		if(offset.delta > ani.delay){
		    			offset.delta = offset.delta - ani.delay;
			    		if(offset.index < ani.objects.length -1){
			    			offset.index++;
			    		}else{
			    			offset.index = 0;
			    		}
		    		
		    		}
		    		
		    		this.context.drawImage(this.img,
							ani.objects[offset.index].x,
							ani.objects[offset.index].y,
							ani.objects[offset.index].w,
							ani.objects[offset.index].h,
							offset.x + (ani.w - ani.objects[offset.index].w)/2,
							offset.y,
							ani.objects[offset.index].w * this.scaleX,
							ani.objects[offset.index].h * this.scaleY);
		    		
		    		}// end for solids 

//###
		    		for(var i = 0; i < this.definedCoins.length; i++){
			    		var offset = this.definedCoins[i];		
			    		
			    		if(typeof offset === "undefined"){
			    			continue;
			    		}

			    		//console.log(this.coords.animation[offset.name]);
			    		var ani = this.coords.animation[offset.name];
			    		
			    		offset.delta += delta;
			    		if(offset.delta > ani.delay){
			    			offset.delta = offset.delta - ani.delay;
				    		if(offset.index < ani.objects.length -1){
				    			offset.index++;
				    		}else{
				    			offset.index = 0;
				    		}
			    		
			    		}
			    		
			    		this.context.drawImage(this.img,
								ani.objects[offset.index].x,
								ani.objects[offset.index].y,
								ani.objects[offset.index].w,
								ani.objects[offset.index].h,
								offset.x + (ani.w - ani.objects[offset.index].w)/2,
								offset.y,
								ani.objects[offset.index].w * this.scaleX,
								ani.objects[offset.index].h * this.scaleY);
			    		} // for coins
//###
		    	
		    		//saida do jogo
		    		var out = this.coords.animation["out"];
		    		
		    		//console.log(out);
		    		
		    		this.context.drawImage(this.img,
		    				out.objects[0].x,
		    				out.objects[0].y,
		    				out.objects[0].w,
		    				out.objects[0].h,
							this.outplace.x + (out.w - out.objects[0].w)/2,
							this.outplace.y,
							out.objects[0].w * this.scaleX,
							out.objects[0].h * this.scaleY);
		    		
		    	} //end if Ready
		    	else{
		    		//Se a imagem não tiver sido carregada .. mostra o contorno
		    		for(var i = 0; i < this.SolidAreas.length; i++){
			    		var area = this.SolidAreas[i];
		    		
			    		if(typeof area !== "undefined"){
			    		
				    		for(var j = 0; j < area.length; j++){
				    			
				    			this.context.rect(
				    					area[j].x,
				    					area[j].y,
				    					area[j].w,
				    					area[j].h);
								this.context.stroke();
				    			
				    		}
			    		
			    		} // if area
			    		
		    	
		    		} // for
	    	
	    	} //else
	    }
	    
	    Scene.prototype.clear = function(){
//###
	    	while(this.definedCoins.length > 0){
				var len = this.definedCoins.length - 1;
	    		delete this.definedCoins[len];
	    		this.definedCoins.length = len;
	    	}
//###
	    	
	    	while(this.definedSolids.length > 0){
				var len = this.definedSolids.length - 1;
	    		delete this.definedSolids[len];
	    		this.definedSolids.length = len;
	    	}
	    	
		    while(this.mobs.length > 0){
				var len = this.mobs.length - 1;
	    		delete this.mobs[len];
	    		this.mobs.length = len;
	    	}
	    }
	    
	    
	  //saber em que posicao esta um ponto
	    Scene.prototype.getIndex = function(scene, _x, _y){
	    	
	    	var col = Math.floor(_x/scene.BlockWidth);
			var row = Math.floor(_y/scene.BlockHeight);
			
			return scene.BlockCols * row + col;
			
	    }
	    
	    /*
	     * Montar a cena aqui..
	     */
	    Scene.prototype.draw = function(delta){
	    	
	    	//*
	    	Clear(this, '#bbbbff');
	    	Grid(this, '#0000ff');
	    	//*/
	    	
	    	/*
	    	Clear('#bbbbff');
	    	Grid('#000000');
	    	//*/
	    	
	    	
	    	//DrawSolids(this); 
	    	this.update(delta);
	    	    	
	    	if(this.player !== null)
	    		this.player.draw(delta);
	    	
	    	for(var i = 0; i < this.mobs.length; i++){
	    		this.mobs[i].draw(delta);
	    	}
	    	
	    	
	    }   

	    return Scene;

	  })();

	  window.Scene = Scene;

	}).call(this);

(function () {
	  var Character;
	  
	  Character = (function() {
	    
		//TODO: definir as animações separadamente
		//TODO: uma animação para cada ação do personagem
	    function Character() {
	    	var that = this;
	    	
	    	
	    	this.img = new Image();
	    	
	    	this.scene = null;
	    	this.animation = null;
	    	
	    	/*
	    	 * 30 frames por segundo * segundo = 30
	    	 * 30/1000 * 10000 = 30;
	    	 * ou seja em 1000 eu vou ter 30;
	    	 * dado um tempo N dividir por 1000
	    	 * se esse numero for ou maior a 30/1000
	    	 * reduzir 30/1000 do numero
	    	 * */
	    	
	    	
	    	//this.delta = 0;
	    	this.fps = 1;
	    	
	    	this.delta = 0;
//	    	this.td = 0;
	    	
	    	
	    	
	    	this.animationIndex = 0;
	    	this.readyState = false;
	    	
	    	
	    	//variaveis auxiliares
	    	var ds;
	    	var dt;
	    	
	    	// n pixels por segundo ao quadrado
	    	ds = 300;
	    	dt = 1000; //1s
	    	this.ACCELERATIONY = ds / (dt * dt); //constante
	    	this.accelerationY = this.ACCELERATIONY; //variavel
	    	this.y = 0;
	    	this.velocityY = 0;
	    	
	    	this.VELOCITY_JUMP  = -300 / 1000;
	    	

	    	// n pixels por segundo
	    	ds = 80;
	    	dt = 1000;  //1s
	    	this.VELOCITYX_WALK = ds / dt; //constante
	    	
	    	ds = 110;
	    	dt = 1000;  //1s
	    	this.VELOCITYX_JUMP = ds / dt; //constante
	    	
	    	
	    	this.velocityX = this.VELOCITYX_WALK;
	    	this.x = 100;
	    	
	    	this.CHARACTER_COLISION_NOPE = false;
	    	this.CHARACTER_COLISION_TOP = false;
	    	this.CHARACTER_COLISION_BOTTOM = false;
	    	this.CHARACTER_COLISION_LEFT = false;
	    	this.CHARACTER_COLISION_RIGHT = false;
	    	this.CHARACTER_MOVE_RIGHT = false;
			this.CHARACTER_MOVE_LEFT = false;
			this.CHARACTER_MOVE_UP = false;
			this.CHARACTER_MOVE_DOWN = false;
			this.CHARACTER_OVERFLOW_GROUND = false;
			this.CHARACTER_OVERFLOW_LEFT = false;
			this.CHARACTER_OVERFLOW_RIGHT = false;
			this.CHARACTER_IS_DEAD = false;
			this.CHARACTER_IS_WON = false;
			this.CHARACTER_READY_STATE = false;
			
			this.CURRENT_ANIMATION = "direita";
	    	
	    	//prototype
	    	this.animations = null;
	    	
	    	once(this.img,'load', function(){
	    		that.CHARACTER_READY_STATE = true;
	    	});
	    	
	    }

	    Character.prototype.setContext = function(Context){
	    	context = Context;
	    }
	    
	    Character.prototype.setScene = function(Scene){
	    	tscene = Scene;
	    }
	    
	    Character.prototype.fitScreen = function(){

	    	//variaveis auxiliares
	    	var ds;
	    	var dt;
	    	
	    	// n pixels por segundo ao quadrado
	    	ds = 300;
	    	dt = 1000; //1s
	    	this.ACCELERATIONY = ds / (dt * dt); //constante
	    	this.accelerationY = this.ACCELERATIONY; //variavel
	    	this.y = 0;
	    	this.velocityY = 0;
	    	
	    	this.VELOCITY_JUMP  = -300 / 1000 * this.scene.scaleY;
	    	

	    	// n pixels por segundo
	    	ds = 80;
	    	dt = 1000;  //1s
	    	this.VELOCITYX_WALK = ds / dt * this.scene.scaleX; //constante
	    	
	    	ds = 110;
	    	dt = 1000;  //1s
	    	this.VELOCITYX_JUMP = ds / dt * this.scene.scaleY; //constante
	    	
	    	
	    	this.velocityX = this.VELOCITYX_WALK;
	    	this.x = 100;
	    	
	    }
 
	    Character.prototype.colideWithPlayer = function (player){
	    	
	    	
	    	if(this.CHARACTER_IS_DEAD || player.CHARACTER_IS_DEAD){
	    		return false;
	    	}
	    	
	    	//adicionar primeiro os pontos da cabeca
	    	var pontos = [
		    	 {
		    		 //cabeca esquerda
		    		 foot : false,
		    		 x: this.x,
		    		 y: this.y
		    	 },
		    	 {
		    		 //cabeca direita
		    		 foot: false,
		    		 x: this.x + (this.w * this.scene.scaleX),
		    		 y: this.y
		    	 },
		    	 {
		    		 //pe direito
		    		 foot: true,
		    		 x: this.x + (this.w * this.scene.scaleX),
		    		 y: this.y + (this.h * this.scene.scaleY)
		    	 },
		    	 {
		    		 //pe esquerdo
		    		 foot: true,
		    		 x: this.x,
		    		 y: this.y + (this.h * this.scene.scaleY)
		    	 }
	    	 ];
	    	
	    	var _cx = this.x + (this.w * this.scene.scaleX / 2);
	    	//var _cy = this.y + (this.h * this.scene.scaleY / 2);
	    	
	    	for(var i = 0; i < 4; i++){
	    		var ponto = pontos[i];
	    		
		    	if ((ponto.x >= player.x) && (ponto.x <= player.x + player.w) && (ponto.y >= player.y) && (ponto.y <= player.y + player.h) ){
		    		
		    		if(ponto.y < toFNgetY(player, 0, _cx)){ //## e\
		    			//left bottom
		    			if(ponto.y > toFNgetY(player, 1, _cx)){ // ## e/
		    				//console.log("L < 0, > 1");
		    				//that.CHARACTER_COLISION_RIGHT = true;
		    				player.CHARACTER_IS_DEAD = true;
		    				player.velocityY = player.VELOCITY_JUMP;
		    				
		    				console.log("1");
		    				return;
		    				
		    			}
		    			else{  // ## /d
		    				//console.log("T < 0, < 1");
		    				
		    				if(ponto.foot){ //identificando que player ponto esta no pe do personagem
		    					//that.CHARACTER_COLISION_BOTTOM = true;
		    					player.CHARACTER_IS_DEAD = true;
		    					player.velocityY = player.VELOCITY_JUMP;
		    					//alert(true);
		    					
		    					console.log("2");
		    					return;
		    					
		    				}
		    				else{
		    					//that.CHARACTER_COLISION_TOP = true;
		    					this.CHARACTER_IS_DEAD = true;
		    					player.velocityY = player.VELOCITY_JUMP;
		    					
		    					console.log(ponto);
		    					
		    					console.log("3");
		    					return;
		    					
		    				}
		    			}
		    		}
		    		else{  //## \d
		    			//top right
		    			if(ponto.y > toFNgetY(player, 1, _cx)){  // ## e/
		    				//console.log("B > 0, > 1");
		    				//that.CHARACTER_COLISION_TOP = true;
		    				this.CHARACTER_IS_DEAD = true;
		    				
		    				console.log("4");
		    				return;
		    				
		    			}
		    			else{  // ## /d
		    				//console.log("R > 0, < 1");
		    				//that.CHARACTER_COLISION_LEFT = true;
		    				
		    				if(ponto.foot){
		    					player.CHARACTER_IS_DEAD = true;
		    					player.velocityY = player.VELOCITY_JUMP;
		    					
		    					console.log("5");
		    					return;
		    					
		    				}
		    				else{
		    					this.CHARACTER_IS_DEAD = true;
		    					player.velocityY = player.VELOCITY_JUMP;
		    					
		    					console.log("6");
		    					return;
		    					
		    				}
		    			}
		    		}
		    		
		    	}  // end if colide
	    	}
	    	return false;
	    }
	    
	    
	    /**
	     * 
	     */
	    Character.prototype.colideWithSolid = function(solid){
	    	
	    	//adicionar primeiro os pontos da cabeca
	    	var pontos = [
		    	 {
		    		 //cabeca esquerda
		    		 foot : false,
		    		 right: false,
		    		 x: this.x,
		    		 y: this.y
		    	 },
		    	 {
		    		 //cabeca direita
		    		 foot: false,
		    		 right: true,
		    		 x: this.x + (this.w * this.scene.scaleX),
		    		 y: this.y
		    	 },
		    	 {
		    		 //pe direito
		    		 foot: true,
		    		 right: true,
		    		 x: this.x + (this.w * this.scene.scaleX),
		    		 y: this.y + (this.h * this.scene.scaleY)
		    	 },
		    	 {
		    		 //pe esquerdo
		    		 foot: true,
		    		 right: false,
		    		 x: this.x,
		    		 y: this.y + (this.h * this.scene.scaleY)
		    	 }
	    	 ];
	    	
	    	var _cx = this.x + (this.w * this.scene.scaleX / 2);
	    	
	    	for(var i = 0; i < 4; i++){
	    		var ponto = pontos[i];
	    		
	    		if(ponto.foot){
	    			this.CHARACTER_COLISION_BOTTOM = false;
	    		}else{
	    			this.CHARACTER_COLISION_TOP = false;
	    		}
	    		
	    		if ((ponto.x >= solid.x) && (ponto.x <= solid.x + solid.w) && (ponto.y >= solid.y) && (ponto.y <= solid.y + solid.h) ){
	    			
	    			
	    			
	    			if(ponto.foot){

						if ((ponto.x >= solid.x) && (ponto.x <= solid.x + solid.w) && (ponto.y >= solid.y) && (ponto.y <= solid.y + 5) ){
		    				this.CHARACTER_COLISION_BOTTOM = true;
		    			}
	    				//else{
	    					//throw "e";
	    				//}
	    			}
	    			
	    			
					this.CHARACTER_COLISION_TOP = true;	    			
	    			
	    			
	    				
    			}

	    	}
	    	
	    }
	        
	    //A colisao é simples
	    //ao colidir com a moeda
	    //1. A moeda vai para o personagem
	    //2. A moeda some: é apagada
	    //3. O stroke some
	    Character.prototype.colideWithCoin = function(coin){
	    	
	    	if(this.type === "mob") return;
	    	
	    	var pontos = [
	    			    	 {
	    			    		 //cabeca esquerda
	    			    		 foot : false,
	    			    		 right: false,
	    			    		 x: this.x,
	    			    		 y: this.y
	    			    	 },
	    			    	 {
	    			    		 //cabeca direita
	    			    		 foot: false,
	    			    		 right: true,
	    			    		 x: this.x + (this.w * this.scene.scaleX),
	    			    		 y: this.y
	    			    	 },
	    			    	 {
	    			    		 //pe direito
	    			    		 foot: true,
	    			    		 right: true,
	    			    		 x: this.x + (this.w * this.scene.scaleX),
	    			    		 y: this.y + (this.h * this.scene.scaleY)
	    			    	 },
	    			    	 {
	    			    		 //pe esquerdo
	    			    		 foot: true,
	    			    		 right: false,
	    			    		 x: this.x,
	    			    		 y: this.y + (this.h * this.scene.scaleY)
	    			    	 }
	    		    	 ];
	    	
	    	
	    	for(var i = 0; i < 4; i++){
	    		var ponto = pontos[i];
		    	if ((ponto.x >= coin.x) && (ponto.x <= coin.x + coin.w) && (ponto.y >= coin.y) && (ponto.y <= coin.y + coin.h) ){
		    		
		    		//if(coin.type ==="out"){ alert("Parabens!, você venceu"); return; }
		    		
		    		delete this.scene.definedCoins[coin.coinIndex];
		    		delete (this.scene.SolidAreas[coin.areaIndex])[coin.itemIndex];
		    	}
	    	}
	    }
	    
	    
	    
	    Character.prototype.colideWithEnd = function(endPlace){
	    	
	    	if(this.type === "mob") return;
	    	
	    	var pontos = [
	    			    	 {
	    			    		 //cabeca esquerda
	    			    		 foot : false,
	    			    		 right: false,
	    			    		 x: this.x,
	    			    		 y: this.y
	    			    	 },
	    			    	 {
	    			    		 //cabeca direita
	    			    		 foot: false,
	    			    		 right: true,
	    			    		 x: this.x + (this.w * this.scene.scaleX),
	    			    		 y: this.y
	    			    	 },
	    			    	 {
	    			    		 //pe direito
	    			    		 foot: true,
	    			    		 right: true,
	    			    		 x: this.x + (this.w * this.scene.scaleX),
	    			    		 y: this.y + (this.h * this.scene.scaleY)
	    			    	 },
	    			    	 {
	    			    		 //pe esquerdo
	    			    		 foot: true,
	    			    		 right: false,
	    			    		 x: this.x,
	    			    		 y: this.y + (this.h * this.scene.scaleY)
	    			    	 }
	    		    	 ];
	    	
	    	
	    	for(var i = 0; i < 4; i++){
	    		var ponto = pontos[i];
		    	if ((ponto.x >= endPlace.x) && (ponto.x <= endPlace.x + endPlace.w) && (ponto.y >= endPlace.y) && (ponto.y <= endPlace.y + endPlace.h) ){
		    		
		    		if(endPlace.type ==="out" && !this.CHARACTER_IS_WON){ alert("Parabens!, você venceu"); this.CHARACTER_IS_WON = true; return; }
		    		
		    		
		    	}
	    	}
	    }
	    
	    
	    
	    
	    
	    Character.prototype.draw = function(delta) {
	    	
	    	if(this.CHARACTER_IS_DEAD){
	    		if(this.type == "player"){
	    			console.log("character is dead");
	    		}
	    	}
	    		    	
	    	//Usado somente nos personagens automaticos
	    	this.updateMotion(delta);
	    	
	    	var o0 = this.coords.animation[this.CURRENT_ANIMATION];
	    	
			this.w = o0.w;
			this.h = o0.h;
						
			var corners = new Array();
			
			corners[0] = this.scene.getIndex(this.scene, this.x, this.y);
			corners[1] = this.scene.getIndex(this.scene, this.x+this.w, this.y);
			corners[2] = this.scene.getIndex(this.scene, this.x, this.y+this.h);
			corners[3] = this.scene.getIndex(this.scene, this.x+this.w, this.y+this.h);
		
			//console.log(this.scene.player);
			if(this.type == "player"){
				this.scene.PlayerAreas = new Array(this.scene.BlockCols * this.scene.BlockRows);
				this.scene.PlayerAreas[corners[0]] = true;
				this.scene.PlayerAreas[corners[1]] = true;
				this.scene.PlayerAreas[corners[2]] = true;
				this.scene.PlayerAreas[corners[3]] = true;
			}else{
//				if(
//						this.scene.PlayerAreas[corners[0]] ||
//						this.scene.PlayerAreas[corners[1]] ||
//						this.scene.PlayerAreas[corners[2]] ||
//						this.scene.PlayerAreas[corners[3]]
//						)
//				{
				this.colideWithPlayer(this.scene.player);
//				} // if player is arround
			}// if not player
			
			
			for(var i = 0; i < 4; i++){
				var objects = this.scene.SolidAreas[corners[i]];
				if(typeof objects !== "undefined"){
					for(var j = 0; j < objects.length; j++){
						
						if(typeof objects[j] !== "undefined"){

							if(objects[j].type === "solid"){
								this.colideWithSolid(objects[j]);
							}
							else if(objects[j].type === "coin"){
								this.colideWithCoin(objects[j]);
							}
							else{
								this.colideWithEnd(objects[j]);
							}

						}

					}//for objects
				}//if objects defined
			}//for each corner
			
			
//			delete corners[0];
//			delete corners[1];
//			delete corners[2];
//			delete corners[3];
			
////######################################################## movimentação: novo

			
			
	    	//Ação da Gravidade
	    	if(!this.CHARACTER_IS_DEAD && (this.CHARACTER_COLISION_BOTTOM || (this.y + this.h >= 6 * this.scene.BlockHeight))){
	    		//A velocidade de caida e constante igual a zero;
	    		this.velocityY = 0;
	    		//this.velocityX = this.VELOCITX_WALK;
	    		
		    	//Pula somente se estiver colidindo no chão
		    	if(this.CHARACTER_MOVE_UP){
		    		this.velocityX = this.VELOCITYX_JUMP;
		    		this.velocityY = this.VELOCITY_JUMP;
		    		this.y = this.y + this.velocityY * delta + this.accelerationY * delta * delta / 2; //falta ajustar o zoom..
		    		this.velocityY = this.velocityY + this.accelerationY * delta; //velocidade variavel;
		    		//esta parte não pode ser usada no nob
		    		//this.CHARACTER_MOVE_UP = false;
				}
	    		
	    	}
	    	else{
//	    		//A velocidade de caida vai aumentando conforme o tempo;
	    		//this.y = this.y + this.velocityY * delta; //falta ajustar o zoom..
	    		this.y = this.y + this.velocityY * delta + this.accelerationY * delta * delta / 2;
		    	this.velocityY = this.velocityY + this.accelerationY * delta; //velocidade variavel;
	    	}
	    	
	    	//se abaixa
	    	if(this.CHARACTER_COLISION_BOTTOM && this.CHARACTER_MOVE_DOWN){
	    		
			}
	    	
	    	//move para a DIREITA somente se não estiver movendo para a esquerda
	    	if(this.CHARACTER_MOVE_RIGHT){
	    		//console.log("this.CHARACTER_MOVE_RIGHT", this.CHARACTER_MOVE_RIGHT);
	    		this.x += this.velocityX * delta;
	    		//TODO: falta ajustar o zoom..
	    		//TODO: Falta atualizar a animação
	    		//console.log(">>>", this.x);
			}
	    	
	    	//move para a ESQUERDA somente se não estiver movendo para a direita
			if(this.CHARACTER_MOVE_LEFT){
				this.x -= this.velocityX * delta;
				//TODO: falta ajustar o zoom..
				//TODO: Falta atualizar a animação
				//console.log("<<<", this.x);
			}
			
////######################################################## movimentação: novo

			
				//desenhar o proximo frame
				if(this.CHARACTER_READY_STATE){
					
					var o1;
					
					//DEBUG
					if(this.animationIndex >= o0.objects.length){
						this.animationIndex = 0;
					}
					o1 = o0.objects[this.animationIndex];
					
					
					if(this.CHARACTER_MOVE_RIGHT || this.CHARACTER_MOVE_LEFT){
						
						try{
							this.scene.context.drawImage(
								this.img,
								o1.x,
								o1.y,
								o1.w,
								o1.h,
								this.x,
								this.y,
								o1.w * this.scene.scaleX,
								o1.h * this.scene.scaleY
							);
						}
						catch(e){
							throw e;
						}
						
						this.delta += delta;						
						if(this.delta > this.fps){
							
							this.delta = this.delta - this.fps;
													
							if(this.animationIndex < o0.objects.length-1){
								this.animationIndex++;
							}
							else{
								//TODO: so passou uma vez no player								
								this.animationIndex = 0;
							}
							
						}

					}
					else{
						this.scene.context.drawImage(
							this.img,
							o1.x,
							o1.y,
							o1.w,
							o1.h,
							this.x,
							this.y,
							o1.w * this.scene.scaleX,
							o1.h * this.scene.scaleY
						);
					}
				}
				else{
					this.scene.context.rect(
						this.x,
						this.y,
						o0.w * this.scene.scaleX,
						o0.h * this.scene.scaleY
					);
					this.scene.context.stroke();	
				}
	    };
	    

	    return Character;

	  })();

	  window.Character = Character;
	  
}).call(this);

(function() {
	  var Player;
	  
	  Player = (function(_super) {
	    __extends(Player, _super);
	    
		var	KEYS_SPACE = 32,
		KEYS_Q = 81,
		KEYS_W = 87,
		KEYS_E = 69,
		KEYS_A = 65,
		KEYS_S = 83,
		KEYS_D = 68,
		KEYS_LEFT = 37,
		KEYS_UP = 38,
		KEYS_RIGHT = 39,
		KEYS_DOWN = 40,
		KEYS_CONTROL = 17;
	    
	    
	    function Player() {
	    	
	    	Player.__super__.constructor.apply(this, arguments);
	    	var that = this;
	    	
	    	this.coords = {"type":"animation-coords","src":"img/mario4.png","width":1400,"height":36,"animation":
	    	{"direita":
	    	{"loops":0,"length":25,"w":26,"h":35,"objects":[
	    		{"id":0,"delay":1,"x":1,"y":1,"w":23,"h":32,"gx":0,"gy":-1,"gz":0},
	    		{"id":1,"delay":1,"x":29,"y":1,"w":24,"h":31,"gx":0,"gy":-2,"gz":0},
	    		{"id":2,"delay":1,"x":58,"y":1,"w":25,"h":30,"gx":-1,"gy":-2,"gz":0},
	    		{"id":3,"delay":1,"x":88,"y":1,"w":25,"h":30,"gx":-1,"gy":-2,"gz":0},
	    		{"id":4,"delay":1,"x":118,"y":1,"w":26,"h":31,"gx":-1,"gy":-2,"gz":0},
	    		{"id":5,"delay":1,"x":149,"y":1,"w":23,"h":32,"gx":-1,"gy":-1,"gz":0},
	    		{"id":6,"delay":1,"x":177,"y":1,"w":22,"h":32,"gx":0,"gy":0,"gz":0},
	    		{"id":7,"delay":1,"x":204,"y":1,"w":19,"h":32,"gx":-1,"gy":-1,"gz":0},
	    		{"id":8,"delay":1,"x":228,"y":1,"w":17,"h":33,"gx":-3,"gy":-1,"gz":0},
	    		{"id":9,"delay":1,"x":250,"y":1,"w":15,"h":33,"gx":-3,"gy":-1,"gz":0},
	    		{"id":10,"delay":1,"x":270,"y":1,"w":15,"h":33,"gx":-3,"gy":-1,"gz":0},
	    		{"id":11,"delay":1,"x":290,"y":0,"w":16,"h":35,"gx":-2,"gy":0,"gz":0},
	    		{"id":12,"delay":1,"x":311,"y":0,"w":17,"h":35,"gx":-2,"gy":0,"gz":0},
	    		{"id":13,"delay":1,"x":333,"y":1,"w":19,"h":34,"gx":-3,"gy":0,"gz":0},
	    		{"id":14,"delay":1,"x":357,"y":1,"w":23,"h":31,"gx":-4,"gy":-2,"gz":0},
	    		{"id":15,"delay":1,"x":385,"y":1,"w":24,"h":30,"gx":-3,"gy":-2,"gz":0},
	    		{"id":16,"delay":1,"x":414,"y":1,"w":24,"h":30,"gx":-3,"gy":-2,"gz":0},
	    		{"id":17,"delay":1,"x":443,"y":1,"w":24,"h":30,"gx":-3,"gy":-2,"gz":0},
	    		{"id":18,"delay":1,"x":472,"y":1,"w":25,"h":31,"gx":-4,"gy":-2,"gz":0},
	    		{"id":19,"delay":1,"x":502,"y":1,"w":24,"h":33,"gx":-5,"gy":-1,"gz":0},
	    		{"id":20,"delay":1,"x":531,"y":1,"w":24,"h":33,"gx":-5,"gy":-2,"gz":0},
	    		{"id":21,"delay":1,"x":560,"y":1,"w":24,"h":34,"gx":-3,"gy":-1,"gz":0},
	    		{"id":22,"delay":1,"x":589,"y":1,"w":22,"h":34,"gx":-2,"gy":0,"gz":0},
	    		{"id":23,"delay":1,"x":616,"y":1,"w":19,"h":34,"gx":-1,"gy":0,"gz":0},
	    		{"id":24,"delay":1,"x":640,"y":1,"w":23,"h":31,"gx":-1,"gy":-2,"gz":0}]},
	    	"esquerda":
	    	{"loops":0,"length":24,"w":26,"h":35,"objects":[
	    		{"id":0,"delay":1,"x":668,"y":1,"w":23,"h":32,"gx":-1,"gy":0,"gz":0},
	    		{"id":1,"delay":1,"x":725,"y":1,"w":25,"h":30,"gx":0,"gy":-1,"gz":0},
	    		{"id":2,"delay":1,"x":755,"y":1,"w":25,"h":30,"gx":0,"gy":-1,"gz":0},
	    		{"id":3,"delay":1,"x":785,"y":1,"w":26,"h":31,"gx":1,"gy":-1,"gz":0},
	    		{"id":4,"delay":1,"x":816,"y":1,"w":23,"h":32,"gx":0,"gy":0,"gz":0},
	    		{"id":5,"delay":1,"x":844,"y":1,"w":22,"h":32,"gx":0,"gy":1,"gz":0},
	    		{"id":6,"delay":1,"x":871,"y":1,"w":19,"h":32,"gx":0,"gy":0,"gz":0},
	    		{"id":7,"delay":1,"x":895,"y":1,"w":17,"h":33,"gx":1,"gy":0,"gz":0},
	    		{"id":8,"delay":1,"x":917,"y":1,"w":15,"h":33,"gx":2,"gy":0,"gz":0},
	    		{"id":9,"delay":1,"x":937,"y":1,"w":15,"h":33,"gx":2,"gy":0,"gz":0},
	    		{"id":10,"delay":1,"x":957,"y":0,"w":16,"h":35,"gx":2,"gy":1,"gz":0},
	    		{"id":11,"delay":1,"x":978,"y":0,"w":17,"h":35,"gx":1,"gy":1,"gz":0},
	    		{"id":12,"delay":1,"x":1000,"y":1,"w":19,"h":34,"gx":2,"gy":2,"gz":0},
	    		{"id":13,"delay":1,"x":1024,"y":1,"w":23,"h":31,"gx":3,"gy":0,"gz":0},
	    		{"id":14,"delay":1,"x":1052,"y":1,"w":24,"h":30,"gx":3,"gy":0,"gz":0},
	    		{"id":15,"delay":1,"x":1081,"y":1,"w":24,"h":30,"gx":3,"gy":0,"gz":0},
	    		{"id":16,"delay":1,"x":1110,"y":1,"w":24,"h":30,"gx":3,"gy":-1,"gz":0},
	    		{"id":17,"delay":1,"x":1139,"y":1,"w":25,"h":31,"gx":3,"gy":0,"gz":0},
	    		{"id":18,"delay":1,"x":1169,"y":1,"w":24,"h":33,"gx":4,"gy":0,"gz":0},
	    		{"id":19,"delay":1,"x":1198,"y":1,"w":24,"h":33,"gx":5,"gy":0,"gz":0},
	    		{"id":20,"delay":1,"x":1227,"y":1,"w":24,"h":34,"gx":3,"gy":1,"gz":0},
	    		{"id":21,"delay":1,"x":1256,"y":1,"w":22,"h":34,"gx":2,"gy":2,"gz":0},
	    		{"id":22,"delay":1,"x":1283,"y":1,"w":19,"h":34,"gx":0,"gy":2,"gz":0},
	    		{"id":23,"delay":1,"x":1307,"y":1,"w":23,"h":31,"gx":0,"gy":0,"gz":0}]}}}
	    	
	    	this.img.src = this.coords.src;
	    	
	    	this.fps = 30;
	    	
	    	this.y = 200;
	    	this.x = 10;
	    		    	
	    	this.keys = [];
	    	once(document, "keydown", function (e) {
	    		switch (e.keyCode) {
	    			case KEYS_SPACE:
	    				that.CHARACTER_MOVE_UP = true;
	    				break;
	    			case KEYS_Q:
	    			case KEYS_W:
	    			case KEYS_E:
	    			case KEYS_A:
	    			case KEYS_S:
	    			case KEYS_D:
	    			case KEYS_LEFT:
	    				that.turnLeft();
	    				break;
	    			case KEYS_UP:
	    			case KEYS_RIGHT:
	    				that.turnRight();
	    				break;
	    			case KEYS_DOWN:
	    			case KEYS_CONTROL:
	    			//thisPlayer.keys[e.keyCode] = e.keyCode;
	    		}
	    		return false;
	    	});

	    	once(document, "keyup", function (e) {
	    		
	    		switch (e.keyCode) {
	    		case KEYS_SPACE:
	    			that.CHARACTER_MOVE_UP = false;
					break;
				case KEYS_Q:
				case KEYS_W:
				case KEYS_E:
				case KEYS_A:
				case KEYS_S:
				case KEYS_D:
				case KEYS_LEFT:
					that.CHARACTER_MOVE_LEFT = false;
					break;
				case KEYS_UP:
				case KEYS_RIGHT:
					that.CHARACTER_MOVE_RIGHT = false;
					break;
				case KEYS_DOWN:
				case KEYS_CONTROL:
	    			//thisPlayer.keys[e.keyCode] = false;
	    		}
	    		return false;
	    	});

	    	
	      return ;
	    }

	    Player.prototype.type = "player";
	    
	    Player.prototype.turnLeft = function(){
	    	if(!this.CHARACTER_MOVE_LEFT){
		    	this.animationIndex = 0;
		    	this.CHARACTER_MOVE_LEFT = true;
		    	this.CURRENT_ANIMATION = "esquerda";
	    	}
	    }
	    
	    Player.prototype.turnRight = function(){
	    	if(!this.CHARACTER_MOVE_RIGHT){
		    	this.animationIndex = 0;
		    	this.CHARACTER_MOVE_RIGHT = true;
		    	this.CURRENT_ANIMATION = "direita";				
			}
	    }
	    
	    Player.prototype.jump = function(){
	    	this.CHARACTER_MOVE_UP = true;
	    }
	    
	    
//    	se essa funcao nao existir vai gerar um erro
	    Player.prototype.updateMotion = function(delta){
    	}
	    
	    //não é necessario colocar o delta
	    
	    return Player;

	  })(Character);
	  
	  window.Player = Player;
	  
}).call(this);

(function() {
	  var Mob;
	  Mob = (function(_super) {
	    __extends(Mob, _super);
	    var that;
	    
	    //o movimento do mob muda toda vez que ele
	    //sair da area de movimentacao
	    
	    //TODO: como colocar um mob em uma area
	    //TODO: como mudar o mob de area
	    function Mob(arg0, arg1) {
	      Mob.__super__.constructor.apply(this, arguments);
	      that = this;
	      
	      this.coords = {"type":"animation-coords","src":"img/mob001.png","width":116,"height":104,"animation":
			{"esquerda":
			{"loops":0,"length":2,"w":26,"h":27,"objects":[
				{"id":0,"delay":10,"x":24,"y":78,"w":26,"h":26,"gx":1,"gy":0,"gz":0},
				{"id":1,"delay":10,"x":52,"y":51,"w":26,"h":27,"gx":0,"gy":0,"gz":0}]},
			"parado":
			{"loops":0,"length":7,"w":30,"h":28,"objects":[
				{"id":0,"delay":10,"x":24,"y":0,"w":24,"h":23,"gx":0,"gy":-2,"gz":0},
				{"id":1,"delay":10,"x":0,"y":23,"w":24,"h":24,"gx":0,"gy":-2,"gz":0},
				{"id":2,"delay":10,"x":24,"y":23,"w":28,"h":28,"gx":0,"gy":-4,"gz":0},
				{"id":3,"delay":10,"x":52,"y":23,"w":28,"h":27,"gx":0,"gy":-4,"gz":0},
				{"id":4,"delay":10,"x":80,"y":23,"w":30,"h":26,"gx":0,"gy":-3,"gz":0},
				{"id":5,"delay":10,"x":24,"y":51,"w":28,"h":27,"gx":0,"gy":-3,"gz":0},
				{"id":6,"delay":10,"x":24,"y":51,"w":28,"h":27,"gx":0,"gy":-3,"gz":0}]},
			"direita":
			{"loops":0,"length":4,"w":24,"h":28,"objects":[
				{"id":0,"delay":10,"x":0,"y":47,"w":20,"h":28,"gx":0,"gy":-4,"gz":0},
				{"id":1,"delay":10,"x":0,"y":75,"w":24,"h":22,"gx":1,"gy":-4,"gz":0},
				{"id":2,"delay":10,"x":52,"y":78,"w":24,"h":23,"gx":-1,"gy":-4,"gz":0},
				{"id":3,"delay":10,"x":78,"y":51,"w":23,"h":25,"gx":-1,"gy":-3,"gz":0}]}}};
	      
	      this.img.src = this.coords.src;
	      
	      this.fps = 400;
	      this.MOB_LEFT = 10;
		  this.MOB_RIGHT = 150;
		  this.FLOOR = false;
		  
		  this.showLines = false;
	      
		  this.delimite = function(col1, row, limitX, distance, translateX, translateY, toRight, showlines){    		  
  		  
			this.showLines = showlines; 
			  
			this.MOB_LEFT = this.scene.BlockWidth *  col1 + limitX;
			this.MOB_RIGHT = this.MOB_LEFT + distance;
			  
			this.y = this.scene.BlockHeight * row + translateY;
			
			if(toRight){
				this.x = this.MOB_LEFT + translateX;
				this.CHARACTER_MOVE_RIGHT = true;
				this.CHARACTER_MOVE_LEFT = false;
				this.CURRENT_ANIMATION = "direita";
				this.animationIndex = 0;
			}
			else{
				this.x = this.MOB_RIGHT - translateX;
				this.CHARACTER_MOVE_RIGHT = false;
				this.CHARACTER_MOVE_LEFT = true;
				this.CURRENT_ANIMATION = "esquerda";
				this.animationIndex = 0;
			}
  		  
		  }
	      
	      this.updateMotion = function(delta){
	    	  
	    	  	//colocar o mob em uma area ou retirar o mob de uma area
	    	  
				if(this.showLines){
					drawLine(this.scene.context,'#eeaa00', this.MOB_LEFT, 0 , this.MOB_LEFT, this.scene.Height);
					drawLine(this.scene.context,'#eeaa00', this.MOB_RIGHT, 0 , this.MOB_RIGHT, this.scene.Height);
				}
	    	  
	    	  
	      		if(this.x + this.w > this.MOB_RIGHT){
	      			this.CHARACTER_MOVE_RIGHT = false;
	      			this.CHARACTER_MOVE_LEFT = true;
	      			this.CURRENT_ANIMATION = "esquerda";
	      			this.animationIndex = 0;
	      		}
	      		
	      		if(this.x < this.MOB_LEFT){
	      			this.CHARACTER_MOVE_RIGHT = true;
	      			this.CHARACTER_MOVE_LEFT = false;
	      			this.CURRENT_ANIMATION = "direita";
	      			this.animationIndex = 0;
	      		}
	      		
	      }
	      
	    }

		Mob.prototype.type = "mob";

	    return Mob;

	  })(Character);
	  //TODO: talvez eu tenha o mesmo problema quando
	  // com a variavel that
	  
	  window.Mob = Mob;

}).call(this);
