class Game
	that=@
	constructor:(canvasId)->
		that=@
		@canvas=document.getElementById CanvasId
		@context=canvas.getContext "2d"
		if typeof once is "function"
			once window,"resize", ->
				that.fitScreen()
	fitScreen: ->
		@canvas.width = window.innerWidth
		@canvas.height = window.innerHeight
		if @scene isnt null
			@scene.makeScene()
		else
			throw "in 'Game' scene is null\nplease use: game.setScene(Scene)"
			return that
	setScene:(@scene)->
		scene.setContext @context
		that.fitScreen()
		return that

	loop:->
		if @scene isnt null
			@scene.draw()
		else
			throw "in 'Game' scene is null\nplease use: game.setScene(Scene)"
		window.requestAnimationFrame that.loop.bind this
