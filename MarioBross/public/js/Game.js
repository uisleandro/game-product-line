'use strict';

//begin game
(function() {
  var Game;
  Game = (function() {
    var that;
    
    var scene = null;
    var canvas = null;
    var context = null;
    
    function Game(CanvasId) {
    	var that = this;
    	
    	this.player = null;
    	
    	this.controls = [];
    	
    	this.mobile = mobilecheck();
    	
    	//Será atualizado
		canvas = document.getElementById(CanvasId);
		
		//Sera atualizado
		context = canvas.getContext("2d");
		
		if(typeof once === "function"){

//###<% if(zoom){ %>
			once(window,'resize', function(){
				//coloca a cena e o canvas do tamanho da janela
				that.fitScreen();
			});
//###<% } %>
			
//###<% if(touch){ %>
	
			var touches = new Array();
			
			//
			
			
	
			var startMoving = function(e){
				for(var j = 0; j < e.touches.length; j++){
					for(var i = 0; i < that.controls.length; i++){
						if(isPointIncircle(e.touches[j].pageX, e.touches[j].pageY, that.controls[i].x, that.controls[i].y, that.controls[i].r)){
							if(that.player){
								if(that.controls[i].action === "LEFT"){
									touches[i] = "LEFT";
									that.player.CHARACTER_MOVE_RIGHT = false;
									that.player.turnLeft();
								}
								else if(that.controls[i].action === "RIGHT"){
									touches[i] = "RIGHT";
									that.player.CHARACTER_MOVE_LEFT = false;
									that.player.turnRight();
								}
								else if(that.controls[i].action === "JUMP_LEFT"){
									touches[i] = "JUMP_LEFT";
									that.player.jump();
								}
								else if(that.controls[i].action === "JUMP_RIGHT"){
									touches[i] = "JUMP_RIGHT";
									that.player.jump();
								}
							} //if
						} // if
					}//for i
				}//for j
				
			};
			
//			//move
//			var processMove = function(e){
//				
//				//se foi pressionado
//				if(that.pressing){
//					that.pressing = false;
//					for(var i = 0; i < that.controls.length; i++){
//						//se ainda esta pressionado
//						if(isPointIncircle(e.x, e.y, that.controls[i].x, that.controls[i].y, that.controls[i].r)){
//							that.pressing = true;
//						}
//					}
//					
//					if(!that.pressing){						
//						that.player.CHARACTER_MOVE_LEFT = false;
//						that.player.CHARACTER_MOVE_RIGHT = false;
//						that.player.CHARACTER_MOVE_UP = false;
//					}
//					
//				}
//				
//			};
			
			
			var endMoving = function(e){
				
				for(var j = 0; j < e.touches.length; j++){
					for(var i = 0; i < that.controls.length; i++){
						if(isPointIncircle(e.touches[j].pageX, e.touches[j].pageY, that.controls[i].x, that.controls[i].y, that.controls[i].r)){
							if(touches[i] === "LEFT"){
								touches[i] = "";
								that.player.CHARACTER_MOVE_LEFT = false;
							}
							else if(touches[i] === "RIGHT"){
								touches[i] = "";
								that.player.CHARACTER_MOVE_RIGHT = false;	
							}
							else if(touches[i] === "JUMP_LEFT"){
								touches[i] = "";
								//that.player.CHARACTER_MOVE_LEFT = false;
								that.player.CHARACTER_MOVE_UP = false;
							}
							else if(touches[i] === "JUMP_RIGHT"){
								touches[i] = "";
								//that.player.CHARACTER_MOVE_RIGHT = false;
								that.player.CHARACTER_MOVE_UP = false;
							}
						}
					}
				}
				
			};
			
			once(canvas, "touchstart", startMoving);
			once(canvas, "touchend", endMoving);
			
//			once(canvas, "touchmove", processMove);

//###<% } %>

//###<% if(mouse){ %>
if(this.mobile){
			var startMouse = function(e){
					for(var i = 0; i < that.controls.length; i++){
						if(isPointIncircle(e.x, e.y, that.controls[i].x, that.controls[i].y, that.controls[i].r)){
							if(that.player){
								if(that.controls[i].action === "LEFT"){
									that.player.turnLeft();
								}
								else if(that.controls[i].action === "RIGHT"){
									that.player.turnRight();
								}
								else if(that.controls[i].action === "JUMP_LEFT"){
									that.player.jump();
								}
								else if(that.controls[i].action === "JUMP_RIGHT"){
									that.player.jump();
								}
							} //if player exists
						} // if inside
					}//for i
			};
						
			var endMouse = function(e){
				
				for(var i = 0; i < that.controls.length; i++){
					if(isPointIncircle(e.x, e.y, that.controls[i].x, that.controls[i].y, that.controls[i].r)){
						if(that.controls[i].action === "LEFT"){
							that.player.CHARACTER_MOVE_LEFT = false;
						}
						else if(that.controls[i].action === "RIGHT"){
							that.player.CHARACTER_MOVE_RIGHT = false;	
						}
						else if(that.controls[i].action === "JUMP_LEFT"){
							//that.player.CHARACTER_MOVE_LEFT = false;
							that.player.CHARACTER_MOVE_UP = false;
						}
						else if(that.controls[i].action === "JUMP_RIGHT"){
							//that.player.CHARACTER_MOVE_RIGHT = false;
							that.player.CHARACTER_MOVE_UP = false;
						}
					}
				}
				
			};
			
			once(canvas, "mousedown", startMouse);
			once(canvas, "mouseup", endMouse);
}
//###<% } %>


		}
		else{
			throw "once is not defined";
			exit(1);
		}
		
		this.now = Date.now();
	    this.then = this.now;
	    
	    this.GAME_RUNNING = false;
	    
	    
		this.setScene(new Scene(this));
		
		this.loop();
	    
		
    }

//###<% if(zoom){ %>
    /**
     * @Description coloca a cena e o canvas do tamanho da janela
     */
    Game.prototype.fitScreen = function(){
    	canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
		
		if(scene != null){
			
			//scene.resize(canvas.width, canvas.height);
			scene.fitScreen();
			scene.clear();
			scene.makeScene();
		}
    	else
    		throw "in 'Game' scene is null\nplease use: game.setScene(Scene)";
		
		
		return that;
    }
//###<% } %>
    
    Game.prototype.setScene = function(Scene){
    	
    	//var that = this;
    	
    	scene = Scene;
    	scene.canvas = canvas;
    	scene.context = context;
    	
//###<% if(zoom){ %>
    	this.fitScreen();
//###<% } %>

//###<% if(touch){ %>
    	this.controls = [
			{ //TR
				x:scene.Width-60,
				y: scene.Height -200,
				r: 50,
				action: "JUMP_RIGHT"
			},
			{ //BR
				x:scene.Width-90,
				y:scene.Height -80,
				r:70,
				action: "RIGHT"
			},    	                 
			{ //TL
				x:60,
				y:scene.Height -200,
				r:50,
				action: "JUMP_LEFT"
			},
			{ //BL
				x:90,
				y:scene.Height -80,
				r:70,
				action: "LEFT"
			}
    	];
//###<% } %>
    	
    	return that;
    }    
    
//###<% if(touch){ %>
    Game.prototype.drawControls = function(){
    	
    	for(var i = 0; i < this.controls.length; i++){
    		Circle(context, this.controls[i].x, this.controls[i].y, this.controls[i].r);
    	}
    }
//###<% } %>
    
    
    Game.prototype.loop = function() {
    	
    	if(!this.GAME_RUNNING){
    		this.GAME_RUNNING = true;
    		
    		this.now = Date.now();
	    	this.then = this.now;
	    	
    		window.requestAnimationFrame(this.loop.bind(this));
    		
    		return;
    	}
    	
    	this.now = Date.now();
    	var delta = (this.now - this.then);
    	this.then = this.now;
    	
    	if(scene != null){
    		//console.log(delta);
    		scene.draw(delta);
//###<% if(touch){ %>
    		this.drawControls();
//###<% } %>
    	}
    	else{
    		throw "in 'Game' scene is null\nplease use: game.setScene(Scene)";
    	}
    	
    	window.requestAnimationFrame(this.loop.bind(this));
    };

    return Game;

  })();

  window.Game = Game;

}).call(this);
